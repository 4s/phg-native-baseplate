/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation of abstract Log module for 4s BasePlate system.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas Lindstrøm</a>,
 * The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "LogModule.hpp"

using namespace s4::messages::classes::log;


namespace s4 {
  namespace BasePlate {
    /**********************************************************************/
    /*                                                                    */
    /*    AbstractLogModule (logging module standard implementation)      */
    /*                                                                    */
    /**********************************************************************/
    
    AbstractLogModule::AbstractLogModule(LogLevel level) {
      this->level = level;
    }
    
    void AbstractLogModule::start() {
      // Create and register a function for the logging messages
      addFunction<Log>("Log", [this](MetaData metadata,
                                            Log* message) -> void {
        
        switch (message->log_level()) {
          case FATAL:
            if (level <= FATAL) {
              fatal(metadata.sender, message->message(),
                    message->file_name(),
                    message->line_number());
            }
            break;
            
          case ERROR:
            if (level <= ERROR) {
              error(metadata.sender, message->message(),
                    message->file_name(),
                    message->line_number());
            }
            break;
            
          case WARN:
            if (level <= WARN) {
              warning(metadata.sender, message->message(),
                      message->file_name(),
                      message->line_number());
            }
            break;
            
          case INFO:
            if (level <= INFO) {
              information(metadata.sender, message->message(),
                          message->file_name(),
                          message->line_number());
            }
            break;
            
          default: // DEBUG
            if (level <= DEBUG) {
              debug(metadata.sender, message->message(),
                    message->file_name(),
                    message->line_number());
            }
            break;
        }
      });
      
      // Start the module through the super class
      ModuleBase::start();
    }
  }
}


