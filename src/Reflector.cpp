/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief The reflector in the 4S PHG C++ 'baseplate' subsystem.
 *
 * The reflector is implemented using ZMQ, where it is usually known as
 * a 'Forwarder'.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *  Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include <zmq.hpp>
#include <atomic>
#include <exception>
#include <iostream>

#include "Reflector.hpp"
#include "Topic.hpp"
#include "BasePlateZMQPAL.hpp"
#include "Common.hpp"

using zmq::context_t;
using zmq::message_t;
using zmq::pollitem_t;
using zmq::socket_t;
using zmq::socket_type;

namespace s4 {
  namespace BasePlate {
    
    /**********************************************************************/
    /*                                                                    */
    /*                Reflector -- internal implementation                */
    /*                                                                    */
    /**********************************************************************/
    
    class Reflector::Underpants {
      friend class Reflector;
      friend class Bridge;
      
      Context* context;
      Reflector* parent;
      
      socket_t *busOut = nullptr;
      socket_t *busIn = nullptr;
      zmq::error_t zmqerror;
      
      std::function<void ()> shutdownCompleteCallback;
      
      enum state_t {
        STATE_NEW,
        STATE_STARTING,
        STATE_PINGING,
        STATE_RUNNING,
        
        STATE_ERROR,
      };
      std::atomic<enum state_t> state;
      
      void changeState(state_t newState) {
        state.store(newState, std::memory_order_release);
      }
      
      state_t getState() {
        return state.load(std::memory_order_relaxed);
      }
      
      /**
       *
       */
      class ReflectorThread final : protected PAL::ModuleThreader {
        friend class Reflector::Underpants;
        Reflector::Underpants *parent;
        ReflectorThread(Reflector::Underpants *parent) : parent(parent) { }
        long init(void) noexcept override { return parent->init(); }
        long handler(zmq::pollitem_t event) noexcept override { return parent->handler(event); }
        long handler() noexcept override { return parent->handler(); }
      } *reflectorThread;
      
      Underpants(Context* context, Reflector* parent)
      : context(context), parent(parent), state(STATE_NEW) {}
      
      ~Underpants() {
        if (getState() == STATE_NEW) {
          // start() was never called
          return;
        }
        
        reflectorThread->join();
        delete reflectorThread;
      }
      
      void start() {
        if (getState() != STATE_NEW) {  // Repeated invocation
          throw std::runtime_error("Repeated start() invocation not allowed.");
        }
        changeState(STATE_STARTING);
        
        // Start switching thread
        reflectorThread = new ReflectorThread(this);
        reflectorThread->start();
      }
      
      long init(void) noexcept {
        // init is called only once - and always in the STARTING state
        assert(!busOut);
        assert(getState() == STATE_STARTING);
        
        // Create and bind sockets for the local reflector
        try {
          busIn = new socket_t(context->context, ZMQ_SUB);
          busOut = new socket_t(context->context, ZMQ_PUB);
          
          busIn->bind(context->module2Reflector);
          busOut->bind(context->reflector2Module);

          busIn->setsockopt(ZMQ_SUBSCRIBE, "", 0);
          
          int lingerTime = 0;
          try {
            busIn->setsockopt(ZMQ_LINGER, &lingerTime, sizeof(lingerTime));
          } catch (...) {}
          try {
            busOut->setsockopt(ZMQ_LINGER, &lingerTime, sizeof(lingerTime));
          } catch (...) {}
          
          pollitem_t pi;
          pi.events = ZMQ_POLLIN;
          pi.socket = *busIn;
          reflectorThread->addPollitem(pi);
          
          changeState(STATE_PINGING);
          
        } catch (zmq::error_t e) {
          // If something went wrong, we transition to the ERROR state
          // and terminate the thread.
          zmqerror = e;
          changeState(STATE_ERROR);
          pollitem_t pi;
          pi.events = ZMQ_POLLIN;
          pi.socket = *busIn;
          reflectorThread->removePollitem(pi);
          if (busIn) delete busIn;
          if (busOut) delete busOut;
        }

        return -1;
      }
      
      void finalize() {
        
        pollitem_t pi;
        pi.events = ZMQ_POLLIN;
        pi.socket = *busIn;
        reflectorThread->removePollitem(pi);

        busIn->close();
        busOut->close();

        delete busIn;
        delete busOut;
      }
      
      void send2all(message_t& data, bool more) const {
        busOut->send(data, more ? ZMQ_SNDMORE : 0);
      }

      long handler(pollitem_t event) noexcept {
        if (event.socket == *busIn && event.revents & ZMQ_POLLIN) {
          message_t msg;
          

          do {
            busIn->recv(&msg);
            string msgString = message2string(msg);
            send2all(msg, msg.more());

            /*
             * FIXME: We need to close the sockets on the reflector thread, but the reflector does not know
             * when the application is finalizing. For now we do just check for the finalize message
             * and act accordingly.
             */
            if (msgString == "B_Shutdown_FINALIZE_") {
              finalize();
            }
          } while (msg.more());
        }
        return -1;
      }
      
      long handler() noexcept {
        pollitem_t pi;
        pi.events = ZMQ_POLLIN;
        pi.socket = *busIn;
        reflectorThread->removePollitem(pi);
        delete busIn;
        delete busOut;
        
        return -1;
      }
    };
    
    /**********************************************************************/
    /*                                                                    */
    /*                   Reflector -- public interface                    */
    /*                                                                    */
    /**********************************************************************/
    
    Reflector::Reflector(Context* context) : underpants(new Underpants(context, this)) {}
          
    Reflector::~Reflector() {
      assert(underpants);
      delete underpants;
      underpants = nullptr;
    }
    
    void Reflector::start() {
      assert(underpants);
      underpants->start();
    }
    
  }
}
