/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an "AS
 *   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 *   express or implied. See the License for the specific language
 *   governing permissions and limitations under the License.
 */

/**
 * @file
 * @brief A Bridging Module of 4S PHG C++ 'baseplate' subsystem
 * implemented using ZeroMQ.
 *
 * A PipingModule is a "normal" module attached to the master
 * baseplate, acting as a proxy for all modules attached to a slave
 * baseplate.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 * Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "PipingModule.hpp"
//#include <functional>
#include <string>
#include <vector>

using namespace s4::BasePlate;
//using std::function;
using std::string;
using std::vector;
using zmq::context_t;
using zmq::message_t;
using zmq::pollitem_t;
using zmq::socket_t;


/**********************************************************************/
/*                                                                    */
/*              PipingModule -- internal implementation               */
/*                                                                    */
/**********************************************************************/

class PipingModule::Underpants {
  friend class PipingModule;

  PipingModule &parent;
  Context &context;
  string endpoint;
  socket_t *sender;
  socket_t *receiver = nullptr;
  
  Underpants(PipingModule &parent, Context &context, peer_t id)
    : parent(parent), context(context), endpoint("inproc://" + id) {
    sender = new socket_t(context.context, ZMQ_PAIR);
    sender->bind(endpoint);

    // Forward the receiver socket input to the reflector
    parent.setExtraHandler([this] (pollitem_t pi) -> void {
        assert(this->receiver);
         
        // Return if no input is available
        if (pi.socket != *(this->receiver)) return;

        message_t msg;
        this->receiver->recv(&msg);
        assert(!msg.more());

        // We ignore 0-length packets
        if (msg.size() == 0) return;
        
        switch(msg.data<int8_t>()[0]) {
        case 2: // SUBSCRIBE
          this->parent.subscribe(msg.data<char>() + 1, msg.size() - 1);
          break;
        case 3: // UNSUBSCRIBE
          this->parent.unsubscribe(msg.data<char>() + 1, msg.size() - 1);
          break;
        default: // Incoming message
          this->parent.sendRaw(msg.data<int8_t>() + 1, msg.size() - 1,
                                msg.data<int8_t>()[0]);
        }
      });
  }

  ~Underpants() {
    assert(sender);
    sender->close();
    delete sender;
  }

  void initialize() {
    assert(!receiver);
    receiver = new socket_t(context.context, ZMQ_PAIR);
    receiver->connect(endpoint);

    pollitem_t pi;
    pi.events = ZMQ_POLLIN;
    pi.socket = *receiver;
    parent.addPollitem(pi);
  }

  void finalize() {
    assert(receiver);
    receiver->close();
    receiver = nullptr;

    pollitem_t pi;
    pi.events = ZMQ_POLLIN;
    pi.socket = *receiver;
    parent.removePollitem(pi);
  }

  void fromBridge(const vector<int8_t> data) {
    assert(sender);
    auto msg = message_t(data.data(), data.size());
    sender->send(msg,0);
  }
};



/**********************************************************************/
/*                                                                    */
/*                 PipingModule -- public interface                   */
/*                                                                    */
/**********************************************************************/

PipingModule::PipingModule(Context& context, peer_t id)
  : ModuleBase(context, id), underpants(new Underpants(*this, context, id)) {}
// start() is called by subclasses


PipingModule::~PipingModule() {
  assert(underpants);
  delete underpants;
  underpants = nullptr;
}

void PipingModule::start() {
  assert(underpants);

  // Hook into the module thread right after connecting the ZMQ
  // sockets and before finalization
  addStateCallback(ApplicationState::INITIALIZING, [this] () -> void {
      this->underpants->initialize();
    });
  addStateCallback(ApplicationState::FINALIZING, [this] () -> void {
      this->underpants->finalize();
    });
  
  // Forward everything from the reflector to the bridge
  setDefaultFunction([this](zmq::message_t& msg) -> void {
      std::vector<int8_t> buffer((int8_t*) msg.data(),
                                 (int8_t*) msg.data() + msg.size());
      // The first byte indicates msg.more
      buffer.insert(buffer.begin(), msg.more() ? 1 : 0);
      this->toBridge(buffer);
    });
  ModuleBase::start();
}

void PipingModule::fromBridge(const vector<int8_t> data) {
  assert(underpants);
  underpants->fromBridge(data);
}
