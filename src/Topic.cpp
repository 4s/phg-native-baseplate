/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an "AS
 *   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 *   express or implied. See the License for the specific language
 *   governing permissions and limitations under the License.
 */

/**
 * @file
 * @brief Topics for messages sent over ZeroMQ int the 4S PHG C++ 'baseplate' subsystem.
 *
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "Topic.hpp"
#include <sstream>
using std::string;

/*
 * Topics are of the form
 *
 * [TYPE][DESTINATION][function]_[SIGNAL]_[SENDER].
 *
 * TYPE is required, and can be one of
 *  U: Unicast. Sent to only one receiver. The DESTINATION must be set.
 *  M: Multicast. Sent to an function. DESTINATION should not be set.
 *  B: Broadcast. Sent to everybody. No other values should be set.
 *
 * function, SIGNAL and SENDER are optional, but SIGNAL cannot be present
 * without the function being present, and SENDER cannot be present without
 * both function and SIGNAL being present.
 *
 * DESTINATION and SENDER are represented by the peer_t type which is an
 * unsigned 32 bit integer.
 */

namespace s4 {
  namespace BasePlate {
    Topic Topic::unicast(peer_t destination, string function, string signal, peer_t sender, string callback) {
      return Topic(UNICAST, destination, function, signal, sender, callback);
    }
    
    Topic Topic::broadcast(string function, string signal) {
      return Topic(BROADCAST, nullID(), function, signal, nullID(), "");
    }
    
    Topic Topic::multicast(string function, string signal, peer_t sender, string callback) {
      return Topic(MULTICAST, nullID(), function, signal, sender, callback);
    }
    
    Topic Topic::subscribeUnicast(peer_t destination) {
      return Topic(UNICAST, destination, "", "", nullID(), "");
    }
    
    Topic Topic::subscribeMulticast(string function) {
      return Topic(MULTICAST, nullID(), function, "", nullID(), "");
    }
    
    Topic Topic::subscribeBroadcast() {
      return Topic(BROADCAST, nullID(), "", "", nullID(), "");
    }
    
    Topic Topic::fromString(string topic) {
      Topic r = Topic(UNICAST, nullID(), "", "", nullID(), "");
      r.parseString(topic);
      return r;
    }
    
    string Topic::stringifyId(peer_t id) {
      //    std::stringstream ss;
      //    ss << std::hex << id;
      //    string res(ss.str());
      //
      //    // We make sure the hex string representation always have the same length
      //    return string(sizeof(peer_t) - res.length(), '0').append(res);
      return id;
    }
    
    peer_t Topic::parseId(string s) {
      //    std::stringstream ss;
      //    peer_t value;
      //    ss << s;
      //    ss >> std::hex >> value;
      //    return value;
      return s;
    }
    
    string Topic::asString() {
      string msg;
      if (type == UNICAST) {
        msg = "U";
        msg += stringifyId(destination);
      } else if (type == MULTICAST) {
        msg = "M";
      } else if (type == BROADCAST) {
        msg = "B";
      }
      
      msg += seperator;
      if (function.empty()) {
        return msg;
      }
      msg += function;
      msg += seperator;
      
      if (signal.empty()) {
        return msg;
      }
      if (signal == "N/A") {
        signal = "";
      }
      msg += signal;
      msg += seperator;
      
      if (sender.empty()) {
        return msg;
      }
      msg += stringifyId(sender);
      msg += seperator;
      
      if (callback.empty()) {
        return msg;
      }
      msg += stringifyId(callback);
      //      msg += seperator;
      
      return msg;
    }
    
    void Topic::parseString(string t) {
      string topic = string(t);
      if (topic[0] == 'U') {
        type = UNICAST;
        size_t i = topic.find(seperator);
        destination = parseId(topic.substr(1, i - 1));
        topic = topic.substr(i + 1);
      } else if (topic[0] == 'M') {
        type = MULTICAST;
        topic = topic.substr(2);
      } else if (topic[0] == 'B') {
        type = BROADCAST;
        topic = topic.substr(2);
      }
      
      size_t i = topic.find(seperator);
      function = topic.substr(0, i);
      if (i == topic.length() - 1) {
        return;
      }
      topic = topic.substr(i + 1);

      i = topic.find(seperator);
      signal = topic.substr(0, i);
      if (i == topic.length() - 1) {
        return;
      }
      topic = topic.substr(i + 1);
      
      i = topic.find(seperator);
      sender = parseId(topic.substr(0, i));
      if (i == topic.length() - 1) {
        return;
      }
      callback = topic.substr(i + 1);

      //      i = topic.find(seperator);
      //     callback = topic.substr(0, i);
    }
  }
}
