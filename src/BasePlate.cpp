/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Implementation of BasePlate.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas Lindstrøm</a>,
 * The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "BasePlate.hpp"
#include "Log.pb.h"

using s4::messages::classes::log::LogLevel;

namespace s4 {
  namespace BasePlate {
    
    class LogOutputModule: virtual public AbstractLogModule {
    public:
      LogOutputModule(Context& context, peer_t id, LogLevel level) :
      ModuleBase(context, id), AbstractLogModule(level) {
        start();
      }
      
      void start() {
        AbstractLogModule::start();
      }
      
      void fatal(peer_t moduleId, string message, string file, int line)
      override {
        std::cerr << BOLDRED << "[Fatal]   " << NORMAL << "(" << moduleId
        << (file.empty() ?
            "" : (":" + file + ":" + std::to_string(line))) << "): "
        << NORMAL << message << std::endl;
      }
      
      void error(peer_t moduleId, string message, string file, int line)
      override {
        std::cerr << BOLDRED << "[Error]   " << NORMAL << "(" << moduleId
        << (file.empty() ?
            "" : (":" + file + ":" + std::to_string(line))) << "): "
        << NORMAL << message << std::endl;
      }
      
      void warning(peer_t moduleId, string message, string file, int line)
      override {
        std::cerr << BOLDRED << "[Warning]   " << NORMAL << "(" << moduleId
        << (file.empty() ?
            "" : (":" + file + ":" + std::to_string(line))) << "): "
        << NORMAL << message << std::endl;
      }
      
      void information(peer_t moduleId, string message, string file, int line)
      override {
        std::cerr << BOLD << "[Info]    " << NORMAL << "(" << moduleId
        << (file.empty() ?
            "" : (":" + file + ":" + std::to_string(line))) << "): "
        << NORMAL << message << std::endl;
      }
      
      void debug(peer_t moduleId, string message, string file, int line)
      override {
        std::cerr << BOLD << "[Debug]   " << NORMAL << "(" << moduleId
        << (file.empty() ?
            "" : (":" + file + ":" + std::to_string(line))) << "): "
        << NORMAL << message << std::endl;
      }
    };
    
    void BasePlate::run(std::set<ModuleBase*> nativeModules,
                        int otherModules, LogLevel logLevel) {
      
      Reflector reflector(&context);
      
      {
        std::vector < std::unique_ptr < ModuleBase >> modules;
        
        for (auto module : nativeModules) {
          modules.emplace_back(module);
        }
        
        // Add Master and Logger
        modules.emplace_back(new LogOutputModule(context, "Logger", logLevel));
        modules.emplace_back(new MasterModule(context, nativeModules.size() + otherModules + 2));
        
        reflector.start();
        
        // The modules will be deleted at the end of this scope, and their threads will be joined,
        // so we will stay here until they have finished.
      }
    }
  }
}

