/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an "AS
 *   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 *   express or implied. See the License for the specific language
 *   governing permissions and limitations under the License.
 */

/**
 * @file
 * @brief The Master Module of 4S PHG C++ 'baseplate' subsystem
 * implemented using ZeroMQ.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 * Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "MasterModule.hpp"
#include <functional>

using std::function;

namespace s4 {
  namespace BasePlate {

    MasterModule::MasterModule(Context& context, int modules) : ModuleBase(context, peer_t(context.masterId)) {
      this->modules = modules;
      start();
    }
    
    /**
     * Check if all moduleId's map to the same given boolean value.
     *
     * @param value A boolean value
     * @return True iff all "Is module i running?" gives the same answer for all modules.
     */
    bool MasterModule::all(bool value) {
      bool all = true;
      for (auto& module : running) {
        if (module.second != value) {
          all = false;
          break;
        }
      }
      return all;
    }

    void MasterModule::addStateCallback(ApplicationState state,
                                        std::function<void()> callback) {
      ModuleBase::addStateCallback(state,callback);
    }

    void MasterModule::requestShutdown(bool forced) const {
      ModuleBase::requestShutdown(forced);
    }
    
    void MasterModule::start() {
      
      /*
       * The Ping function listens after Ping messages and respond to the sender if.
       */
      addFunction("StartupMaster", [this](MetaData metadata) -> void {

        if (metadata.signal == "PING") {
          if (!running.count(metadata.sender)) {
            running[metadata.sender] = false;
            dbg("Got PING from " + metadata.sender + ", " + std::to_string(running.size()) + "/" + std::to_string(this->modules));
          } else {
            dbg("Got repeated PING from " + metadata.sender);
          }
          sendUnicast(metadata.sender, "Startup", "PONG");

        } else if (metadata.signal == "READY") {
          
          if (!running.count(metadata.sender)) {
            err("Unexpected READY signal from unknown module '"
                             + metadata.sender + "' will be ignored.");
            return;
          }
          
          running[metadata.sender] = true;
          dbg(metadata.sender + " is ready");
          
          bool allReady = all(true);
          
          if (running.size() == (size_t)this->modules && allReady) {
            ModuleBase::sendBroadcast("Startup", "START");
            dbg("Got READY signal from all " + std::to_string(running.size())
                              + " modules. Starting the application.");
          }
        }
      });
      
      /* The ShutdownOrchestrator function listens for shutdown REQUESTs. When such a request is received, it is broadcast to all modules. These now have to send either READY or CANCEL. If all responds with READY, a FINALIZE signal is broadcast to all modules which then have to shut down. If one module answers with a CANCEL signal, this is broadcast to all modules and the shutdown process is cancelled. */
      addFunction("ShutdownMaster", [this](MetaData metadata) -> void {
        
        if (metadata.signal == "FORCE") {
          // Force shutdown without request
          ModuleBase::sendBroadcast("Shutdown", "FINALIZE");
          for (auto peer : running) {
            peer.second = false;
          }

        } else if (metadata.signal == "REQUEST") {
          // Request modules to shutdown
          ModuleBase::sendBroadcast("Shutdown", "REQUEST");
        } else if (metadata.signal == "READY") {
          if (running.count(metadata.sender)) {
            running[metadata.sender] = false;
            
            bool allShutdown = all(false);
            
            if (allShutdown) {
              ModuleBase::sendBroadcast("Shutdown", "FINALIZE");
            }
          }
          
        } else if (metadata.signal == "CANCEL") {
          
          dbg(metadata.sender + " cancelled shutdown");
          for (auto peer : running) {
            peer.second = true;
          }
          ModuleBase::sendBroadcast("Shutdown", "CANCEL");
        }
      });
      
      ModuleBase::start();
    }
  }
}
