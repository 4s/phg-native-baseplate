/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief One-thread-per-module implementation of the ModuleThreader.
 *
 * This file implements the ModuleThreader interface by creating a new
 * thread for each module.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "BasePlateZMQPAL.hpp"
#include <list>
#include <cassert>
#include <map>
#include <thread>
#include <vector>
#include <iostream>


using namespace s4::BasePlate::PAL;
using std::list;
using std::map;
using std::runtime_error;
using std::thread;
using std::vector;
using zmq::poll;
using zmq::pollitem_t;




/**********************************************************************/
/*                                                                    */
/*             ModuleThreader -- internal implementation              */
/*                                                                    */
/**********************************************************************/


class ModuleThreader::Underpants final {
  friend class ModuleThreader;

  ModuleThreader *parent;
  thread *theThread = nullptr;
  map<void*,short> socketPollitems;
  map<int,short> fdPollitems;
  vector<pollitem_t> pollitems;
  std::function<void(void)> finalCallback;

  Underpants(ModuleThreader *parent) : parent(parent) {};

  ~Underpants() {
    assert(!theThread);
  }
  
  void run() {
    long timeout;
    
    // Ready to initialize the derived part of the object.
    timeout = parent->init();
    if (timeout < 0) timeout = -1;
    
    while (timeout>=0 || pollitems.size()) {
      //std::cout << ".";
      int count;
      try {
        count = poll(pollitems, timeout);
      } catch (zmq::error_t e) {
        count = -1;

        // Exception!
        // Remove all registered pollitems which will cause
        // the thread to exit if the exception is left unhandled
        
        socketPollitems.clear();
        fdPollitems.clear();
        pollitems.clear();

        // Exception handler
        timeout = parent->handler(e);
      }
      if (count < 0) { // exception already handled above
      } else if (!count) { // timeout
        timeout = parent->handler();
        if (timeout < 0) timeout = -1;
      } else { // Iterate through events
        
        // Default timeout (longest possible - indefinite)
        timeout = -1;

        // Build a local list of events we need to signal (since a
        // handler may alter the pollitems list, so we cannot trust a
        // pollitems iterator would survive a handler call)
        list<pollitem_t> events;
        for (auto it : pollitems) {
          if (it.revents) {
            events.push_back(it);
          }
        }

        // Now, invoke those handlers. The new timeout value will be
        // the shortest timeout requested from those handlers.
        for (auto it : events) {
          long t = parent->handler(it);
          if (t >= 0 && t < timeout) timeout = t;
        }
      }      
    }

    if (finalCallback) {
      finalCallback();            
    }
  }
  
  void setFinalCallback(std::function<void(void)> callback) {
    finalCallback = callback;
  }
  
  void start() {
    if (theThread) throw runtime_error("Thread already started.");
    theThread = new thread([this]()->void {this->run();});
  }

  void join() {
    if (!theThread) throw runtime_error("Thread already stopped.");
    theThread->join();
    delete theThread;
    theThread = nullptr;
  }

  void updatePollitems() {
    pollitems.resize(socketPollitems.size() + fdPollitems.size());
    int i = 0;
    for (auto it : socketPollitems) {
      pollitems[i].socket = it.first;
      pollitems[i].fd = 0;
      pollitems[i].events = it.second;
      i++;
    }
    for (auto it : fdPollitems) {
      pollitems[i].socket = nullptr;
      pollitems[i].fd = it.first;
      pollitems[i].events = it.second;
      i++;
    }
  }
  
  void addPollitem(pollitem_t pi) {
    if (pi.socket) {
      socketPollitems[pi.socket] |= pi.events;
    } else if (pi.fd) {
      fdPollitems[pi.fd] |= pi.events;
    }
    updatePollitems();
  }
  
  void removePollitem(pollitem_t pi) {
    if (pi.socket) {
      if(!(socketPollitems[pi.socket] &= ~pi.events))
        socketPollitems.erase(pi.socket);
    } else if (pi.fd) {
      if(!(fdPollitems[pi.fd] &= ~pi.events))
        fdPollitems.erase(pi.fd);
    }
    updatePollitems();
  }

  
  size_t getPollitems(pollitem_t const **items) {
    *items = pollitems.data();
    return pollitems.size();
  }

};




/**********************************************************************/
/*                                                                    */
/*               ModuleThreader -- public interface                   */
/*                                                                    */
/**********************************************************************/


ModuleThreader::ModuleThreader() : underpants(new Underpants(this)) {}

ModuleThreader::~ModuleThreader() {
  assert(underpants);
  delete underpants;
  underpants = nullptr;
}

void ModuleThreader::start() {
  assert(underpants);
  underpants->start();
}

void ModuleThreader::join() {
  assert(underpants);
  underpants->join();
}

void ModuleThreader::addPollitem(pollitem_t pi) {
  assert(underpants);
  underpants->addPollitem(pi);
}

void ModuleThreader::removePollitem(pollitem_t pi) {
  assert(underpants);
  underpants->removePollitem(pi);  
}

size_t ModuleThreader::getPollitems(pollitem_t const **items) {
  assert(underpants);
  return underpants->getPollitems(items);  
}

void ModuleThreader::setFinalCallback(std::function<void(void)> function) {
  assert(underpants);
  underpants->setFinalCallback(function);
}
