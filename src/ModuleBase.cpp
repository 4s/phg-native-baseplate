/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an "AS
 *   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 *   express or implied. See the License for the specific language
 *   governing permissions and limitations under the License.
 */

/**
 * @file
 * @brief The 4S PHG C++ 'baseplate' subsystem implemented using ZeroMQ.
 *
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include <atomic>
#include <exception>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <utility>
#include <vector>
#include "BasePlate.hpp"
#include "ModuleBase.hpp"
#include "MasterModule.hpp"
#include "Reflector.hpp"
#include "BasePlateZMQPAL.hpp"
#include "Topic.hpp"
#include "Error.pb.h"
#include "Log.pb.h"

using namespace s4::BasePlate;
using namespace s4::BasePlate::PAL;
using s4::messages::classes::log::Log;
using s4::messages::classes::log::LogLevel;
using s4::messages::classes::error::Error;
using google::protobuf::MessageLite;
using std::atomic;
using std::function;
using std::map;
using std::memory_order_relaxed;
using std::memory_order_release;
using std::pair;
using std::runtime_error;
using std::set;
using std::string;
using std::vector;
using zmq::context_t;
using zmq::message_t;
using zmq::pollitem_t;
using zmq::socket_t;
using zmq::socket_type;

/**********************************************************************/
/*                                                                    */
/*               ModuleBase -- internal implementation                */
/*                                                                    */
/**********************************************************************/

class ModuleBase::Underpants {
  friend class ModuleBase;
  
  Context& context;
  ApplicationState state = INITIALIZING;
  enum { IDLE, PING, PING_ACK } connectionState = IDLE;
  peer_t myId;
  bool hasDefault = false;

  socket_t *busOut = nullptr;
  socket_t *busIn = nullptr;
  
  bool errorDuringSubclassInit = false;
  
  uint64_t uid = 0;
  
  map<ApplicationState, function<void()>> stateCallbacks;
  
  map<string, std::unique_ptr<Function>> functions;
  std::function<void(zmq::message_t&)> defaultFunction = [](zmq::message_t&) -> void {
    // Do nothing by default
  };
  std::function<void(zmq::pollitem_t)> extraHandler = [](zmq::pollitem_t) -> void {
    // Do nothing by default
  };
  
  // Default shutdown request handler just accepts any request.
  function<bool(void)> shutdownRequestHandler = []() -> bool {
    return true;
  };
  
  class ModuleThread final : protected PAL::ModuleThreader {
    friend class ModuleBase::Underpants;
    friend class ModuleBase;
    ModuleBase::Underpants &parent;
    ModuleThread(ModuleBase::Underpants &parent) : parent(parent) { }
    
    long init(void) noexcept override {
      return parent.init();
    }
    long handler(zmq::pollitem_t event) noexcept override {
      return parent.handler(event);
    }
    long handler() noexcept override { return parent.handler(); }
  } * moduleThread;
  
  Underpants(Context& context, peer_t id) : context(context), myId(id) {
    //    this->busReady();
  }
  
  ~Underpants() {
    moduleThread->join();
    delete moduleThread;
    
    // To make sure that sockets are closed in the right order (modules, reflector, context), we close
    // the sockets in the desctrutor and not on the module thread.
    busIn->close();
    busOut->close();
    delete busIn;
    delete busOut;
  }
  
  peer_t getId() {
    return myId;
  }
  
  void addStateCallback(ApplicationState state, function<void(void)> callback) {
    stateCallbacks[state] = callback;
  }
  
  void onInitializing() {
    // Not used yet
  }
  
  void onReady() {
    // Not used yet
  }
  
  void onRunning() {
    // Not used yet
  }
  
  void setDefaultFunction(std::function<void(zmq::message_t&)> function) {
    // Needs to be done before init.
    assert(!busIn);

    hasDefault = true;
    this->defaultFunction = function;
  }

  void setExtraHandler(std::function<void(zmq::pollitem_t)> function) {
    // Needs to be done before init.
    assert(!busIn);

    this->extraHandler = function;
  }

  void setFinalCallback(function<void()> function) {
    moduleThread->setFinalCallback(function);
  }
  
  void setShutdownRequestHandler(function<bool(void)> handler) {
    this->shutdownRequestHandler = handler;
  }
  
  void onShutdownRequest() {
    if(shutdownRequestHandler()) {
      sendUnicast(context.masterId, "ShutdownMaster", "READY");
    } else {
      sendUnicast(context.masterId, "ShutdownMaster", "CANCEL");
    }
  }
  
  void onFinalizing() {
    // No more communication is allowed, so we remove the only pollitem from the module thread
    pollitem_t pi;
    pi.events = ZMQ_POLLIN;
    pi.socket = *busIn;
    moduleThread->removePollitem(pi);
  }
  
  void applicationStateChanged(ApplicationState newState) {
    switch (newState) {
      case INITIALIZING:
        onInitializing();
        break;
        
      case READY:
        onReady();
        break;
        
      case RUNNING:
        onRunning();
        break;
        
      case SHUTDOWN_REQUEST:
        onShutdownRequest();
        break;
        
      case FINALIZING:
        onFinalizing();
        break;
    }
    if (stateCallbacks.count(newState)) {
      stateCallbacks[state]();
    }
  }
  
  void start() {

    busReady();
    // Add default functions that all modules should have.
    addFunction("Startup", new Procedure([this](MetaData metadata) -> void {
      if (state == INITIALIZING &&
          metadata.signal == "PONG") {
        
        changeState(READY);
        dbg(myId + " got PONG.");
        
        // Once a module receives a PONG back from the Master, it can send
        // a READY state back to the StartupOrchestrator.
        connectionState = PING_ACK;
        
        sendUnicast(context.masterId, "StartupMaster", "READY");
        
      } else if (state == READY &&
                 metadata.signal == "START") {
        
        // We got the START signal.
        if (errorDuringSubclassInit) {
          err("Unhandled exception in applicationStateChange(INITIALIZING) "
              "handler.");
        }
        changeState(RUNNING);
        dbg(myId + " is ready");
      }
    }));
    
    addFunction("Shutdown", new Procedure([this](MetaData metadata) -> void {
      if (metadata.signal == "REQUEST") {
        changeState(SHUTDOWN_REQUEST);
      } else if (state == SHUTDOWN_REQUEST && metadata.signal == "CANCEL") {
        changeState(RUNNING);
      } else if (metadata.signal == "FINALIZE") {
        changeState(FINALIZING);
      }
    }));
  }
  
  /**
   * Start the module thread
   */
  void busReady() {
    assert(state == INITIALIZING);
    
    // Start module thread
    moduleThread = new ModuleThread(*this);
    moduleThread->start();
  }
  
  void subscribe(const char *prefix, size_t len) {
    busIn->setsockopt(ZMQ_SUBSCRIBE, prefix, len);
  }
  
  void subscribe(string prefix) {
    subscribe(prefix.c_str(), prefix.length());
  }
  
  void subscribe(Topic topic) {
    subscribe(topic.asString());
  }
  
  void unsubscribe(const char *prefix, size_t len) {
    busIn->setsockopt(ZMQ_UNSUBSCRIBE, prefix, len);
  }

  void unsubscribe(string prefix) {
    unsubscribe(prefix.c_str(), prefix.length());
  }

  void unsubscribe(Topic topic) {
    unsubscribe(topic.asString());
  }
  
  long init(void) noexcept {
    // init is called only once - and always in the INITIALIZING state
    assert(!busOut && state == INITIALIZING);
    
    // Create and connect sockets to the local reflector
    try {
      busOut = new socket_t(context.context, ZMQ_PUB);
      busIn = new socket_t(context.context, ZMQ_SUB);
      
      busOut->connect(context.module2Reflector);
      busIn->connect(context.reflector2Module);

      int lingerTime = 0;
      try {
        busIn->setsockopt(ZMQ_LINGER, &lingerTime, sizeof(lingerTime));
      } catch (...) {}
      try {
        busOut->setsockopt(ZMQ_LINGER, &lingerTime, sizeof(lingerTime));
      } catch (...) {}
      
      // Subscribe to all broadcast messages
      subscribe(Topic::subscribeBroadcast());
      
      // Subscribe to all unicast messages to me
      subscribe(Topic::subscribeUnicast(myId));
      
      // Subscribe to multicasts to this modules registered functions
      for (auto &d : functions) {
        subscribe(Topic::subscribeMulticast(d.first));
      }
      
      pollitem_t pi;
      pi.events = ZMQ_POLLIN;
      pi.socket = *busIn;
      moduleThread->addPollitem(pi);
    } catch (zmq::error_t e) {
      // Nothing much we can do about it - logging is not ready...
      // However, the only exception that should be likely at this stage is
      // an invalid or closed 0MQ context.
      // In both cases that would mean that we should bail out!
      pollitem_t pi;
      pi.events = ZMQ_POLLIN;
      pi.socket = *busIn;
      moduleThread->removePollitem(pi);
      if (busIn) delete busIn;
      if (busOut) delete busOut;
      return -1;
    }
    
    // Reflector exists, so we proceed to initialize the module on
    // this thread
    try {
      // Initialize subclass on the correct thread
      changeState(INITIALIZING);
    } catch (...) {
      // Uncaught exception will be reported as soon as logging is
      // available
      errorDuringSubclassInit = true;
    }
    
    // We start testing the reflector connection with a 1 ms timeout
    // until we have a connection
    sendPing();
    connectionState = PING;
    
    // Retry after waiting no more than 1 ms.
    return 1;
  }
  
  /**
   * Send ping message to the Master Module.
   */
  void sendPing() {
    sendUnicast(context.masterId, "StartupMaster", "PING");
    dbg(myId + " sent PING");
  }

  long handler(pollitem_t event) {
    
    if (event.socket == *busIn && event.revents & ZMQ_POLLIN) {

      message_t msg;

      switch (state) {
          
        case INITIALIZING:
        case READY:
        case RUNNING:
        case SHUTDOWN_REQUEST: {
          /*
           * We expect messages which are either 1- or 2-part with the first being the topic and the second 
           * (if present) is a Google Protobuf message serialized as a string. Any more parts will be ignored.
           */

          // Receive message(s)
          string topicString, dataString;

          busIn->recv(&msg);
          defaultFunction(msg);
          topicString = message2string(msg);

          bool hasData = msg.more();
          if (hasData) {
            busIn->recv(&msg);
            defaultFunction(msg);
            dataString = message2string(msg);
          }

          // Handle message(s)
          Topic topic = Topic::fromString(topicString);

          string functionName = topic.function;

          if (functionName[0] == '#') {
            if (topic.signal[0] == 'O') {
              functionName = functionName + "_OK.";
            } else {
              functionName = functionName + "_ERR.";
            }
          }
          
          if (functions.count(functionName)) {
            
            MetaData metadata = MetaData{topic.type, topic.signal, topic.sender, topic.callback};
            MessageLite* input = nullptr;
            
            // If the function expects data, we attempt to receive it
            if (functions[functionName]->expectsData()) {
              if (hasData) {
                input = functions[functionName]->getInstance();
                input->ParseFromString(dataString);
              } else {
                err("The message with topic " + topicString + " did not contain any data even though the function " + topic.function + " expected it.");
                return -1;
              }
            }

            
            // These callbacks are used when the function is called below, so the topic variable is still in scope.
            // TODO: Currently the callbacks are always constructed, even when they are not used or relevant.
            ErrorCallback error([this, topic](int code, std::string msg, bool keepAlive) -> void {
              if (topic.callback.empty()) {
                err("Function " + topic.function + " on module " + myId + " attempted to send an error (" + msg + ") to "+ topic.sender + " but no callback interface was given.");
              } else {
                Error errorMessage;
                errorMessage.set_message(msg);
                errorMessage.set_error_code(code);
                err(msg);
                
                // If this callback should be kept alive, we indicate it in the signal parameter
                std::string signal = keepAlive ? "KEEP" : "LAST";
                this->sendUnicast(topic.sender,
                                  "ERR" + topic.callback,
                                  signal,
                                  &errorMessage);
              }
            });
            
            Callback callback([this, topic, error](google::protobuf::MessageLite* msg, bool keepAlive) -> void {
              if (topic.callback.empty()) {
                err("Function " + topic.function + " on module " + myId + " attempted to respond to "
                    + topic.sender + " but no callback interface was given.");
              } else {
                // If this callback should be kept alive, we indicate it in the signal parameter
                std::string signal = keepAlive ? "KEEP" : "LAST";
                this->sendUnicast(topic.sender, topic.callback, signal, msg);
              }
            });
            
            // Apply the function
            try {
              functions[functionName]->apply(metadata, input, callback, error);
            } catch(const std::runtime_error& re) {
              // On any oncaught exception or runtime error we shut down.
              ftl(re.what());
              requestShutdown(true);
              return -1;
            } catch(const std::exception& ex) {
              ftl(ex.what());
              requestShutdown(true);
              return -1;
            }
            
            // If an input was received, we delete it again
            if (input) {
              delete input;
              input = nullptr;
            }
          }
          
          break;
        }
          
        case FINALIZING: {
          // Ignore messages
          break;
        }
      }
      

      // If not all messages has been received at this point, we report an error
      if (msg.more()) {
        err("Message contained more than the handler " + myId + " expected.");
        do {
          busIn->recv(&msg);
          err("Un-read message: " + message2string(msg));
        } while (msg.more());
      }
      msg = message_t();  // Reset the more() flag
    } else if (event.revents & ZMQ_POLLIN) {
      // event.socket != *busIn then invoke the extraHandler
      extraHandler(event);
    }
    
    return handler();
  }
  
  long handler() {
    if (state == INITIALIZING && connectionState == PING) {
      // We are in the PING state during connection set-up
      // Keep retransmitting PING until we receive it back
      sendPing();
      return 1;
    }
    return -1;
  }
  
  void log(LogLevel type, string message, string file = "", int line = 0) const noexcept {
    Log logMessage;
    logMessage.set_message(message);
    logMessage.set_log_level(type);
    
    if (!file.empty()) {
      logMessage.set_file_name(file);
    }
    
    if (line > 0) {
      logMessage.set_line_number(line);
    }
    
    sendMulticast("Log", "MyLog", &logMessage);
  }
  
  void changeState(ApplicationState newState) noexcept {
    state = newState;
    try {
      this->applicationStateChanged(newState);
    } catch (...) {
      string stateText;
      
      switch (newState) {
        case INITIALIZING:
          stateText = "INITIALIZING";
          break;
          
        case READY:
          stateText = "READY";
          break;
          
        case RUNNING:
          stateText = "RUNNING";
          break;
          
        case SHUTDOWN_REQUEST:
          stateText = "SHUTDOWN_REQUEST";
          break;
          
        case FINALIZING:
          stateText = "FINALIZING";
          break;
      }
      err("Unhandled exception in applicationStateChange(" + stateText +
          ") handler.");
    }
  }
  
  void requestShutdown(bool force) {
    // Request the reflector module to initiate a shutdown
    if (force) {
      sendUnicast(context.masterId, "ShutdownMaster", "FORCE");
      dbg(myId + " forces shutdown\n");
    } else {
      sendUnicast(context.masterId, "ShutdownMaster", "REQUEST");
      dbg(myId + " requests shutdown\n");
    }
  }
  
  void cancelShutdown() {
    // Request that the ongoing shutdown is cancelled
    assert(state == SHUTDOWN_REQUEST);
    sendUnicast(context.masterId, "ShutdownMaster", "CANCEL");
    dbg(myId + " cancels shutdown\n");
  }
  
  void addFunction(string name, Function* function) {
    // Overwrite existing functions with the same name
    functions[name] = std::unique_ptr<Function>(function);
  }
  
  void removeFunction(string name) {
    if (!functions.count(name)) {
      throw runtime_error("No function with name '" + name + "' has been added.");
    }
    
    if (!busIn) {
      throw runtime_error("Message queue not yet initialized.");
    }
    
    functions.erase(name);
  }
  
  void sendUnicast(peer_t destination,
                   string interface,
                   string signal,
                   MessageLite* arg = nullptr) const {
    Topic topic = Topic::unicast(destination, interface, signal, myId);
    sendMessage(topic, arg);
  }

  void sendUnicast(peer_t destination,
                   string interface,
                   MessageLite* arg = nullptr) const {
    Topic topic = Topic::unicast(destination, interface, "N/A", myId);
    sendMessage(topic, arg);
  }
  
  string createUID() {
    return std::to_string(uid++);
  }
  
  void sendUnicastWithCallback(peer_t destination,
                               string interface,
                               string signal,
                               string callback,
                               MessageLite* arg = nullptr) {
    Topic topic = Topic::unicast(destination, interface, signal, myId, callback);
    sendMessage(topic, arg);
  }

  void sendMulticast(string interface,
                     string signal,
                     MessageLite* arg = nullptr) const {
    Topic topic = Topic::multicast(interface, signal, myId);
    sendMessage(topic, arg);
  }
  
  void sendMulticastWithCallback(string interface,
                                 string signal,
                                 string callback,
                                 MessageLite* arg = nullptr) {
    Topic topic = Topic::multicast(interface, signal, myId, callback);
    sendMessage(topic, arg);
  }
  
  void sendBroadcast(string interface,
                     string signal,
                     MessageLite* arg = nullptr) const {
    Topic topic = Topic::broadcast(interface, signal);
    sendMessage(topic, arg);
  }
  
  void sendMessage(Topic topic, MessageLite* data) const {
    string msg = topic.asString();
    bool hasData = bool(data);
    sendRaw(msg, hasData);

    if (hasData) {
      string m = data->SerializeAsString();
      sendRaw(m, false);
    }
  }
  
  void sendRaw(string data, bool hasMore) const {
    busOut->send(string2message(data), hasMore ? ZMQ_SNDMORE : 0);
  }
  
  void sendRaw(std::vector<int8_t> data, bool hasMore) const {
    auto msg = message_t(data.data(), data.size());
    busOut->send(msg, hasMore ? ZMQ_SNDMORE : 0);
  }
  
  void sendRaw(int8_t* data, size_t size, bool hasMore) const {
    auto msg = message_t(data, size);
    busOut->send(msg, hasMore ? ZMQ_SNDMORE : 0);
  }

  string createCallbackUID() {
    string uid = "CALLBACK" + createUID();
    return uid;
  }
};

/**********************************************************************/
/*                                                                    */
/*                  ModuleBase -- public interface                    */
/*                                                                    */
/**********************************************************************/

ModuleBase::ModuleBase(Context& context, peer_t id) : underpants(new Underpants(context, id)) {}

ModuleBase::~ModuleBase() {
  assert(underpants);
  delete underpants;
  underpants = nullptr;
}

peer_t ModuleBase::getId() const {
  assert(underpants);
  return underpants->getId();
}

void ModuleBase::start() {
  assert(underpants);
  underpants->start();
}

void ModuleBase::log(LogLevel level,
                     string msg,
                     string file,
                     int line) const noexcept {
  underpants->log(level, msg, file, line);
}

ApplicationState ModuleBase::getApplicationState() {
  assert(underpants);
  return underpants->state;
}

void ModuleBase::requestShutdown(bool force) const {
  assert(underpants);
  underpants->requestShutdown(force);
}

void ModuleBase::addFunction(string name, Function* function) {
  assert(underpants);
  return underpants->addFunction(name, function);
}

void ModuleBase::setDefaultFunction(function<void(zmq::message_t&)> function) {
  assert(underpants);
  return underpants->setDefaultFunction(function);
}

void ModuleBase::setFinalCallback(function<void()> function) {
  assert(underpants);
  return underpants->setFinalCallback(function);
}

void ModuleBase::removeFunction(string name) {
  assert(underpants);
  return underpants->removeFunction(name);
}

void ModuleBase::sendBroadcast(string interface,
                               string signal,
                               MessageLite* arg) const {
  assert(underpants);
  underpants->sendBroadcast(interface, signal, arg);
}

void ModuleBase::sendMulticast(string interface,
                               string signal,
                               MessageLite* arg) const {
  assert(underpants);
  underpants->sendMulticast(interface, signal, arg);
}

void ModuleBase::sendMulticastWithCallback(string interface,
                                           string signal,
                                           string callback,
                                           MessageLite* arg) const {
  assert(underpants);
  underpants->sendMulticastWithCallback(interface, signal, callback, arg);
}

void ModuleBase::sendUnicast(peer_t destination,
                             string interface,
                             string signal,
                             MessageLite* arg) const {
  assert(underpants);
  underpants->sendUnicast(destination, interface, signal, arg);
}

void ModuleBase::sendUnicastWithCallback(peer_t destination,
                                         string interface,
                                         string signal,
                                         string callback,
                                         MessageLite* arg) const {
  assert(underpants);
  underpants->sendUnicastWithCallback(destination, interface, signal, callback, arg);
}

void ModuleBase::sendRaw(string data, bool more) const {
  assert(underpants);
  underpants->sendRaw(data, more);
}

void ModuleBase::sendRaw(std::vector<int8_t> data, bool more) const {
  assert(underpants);
  underpants->sendRaw(data, more);
}

void ModuleBase::sendRaw(int8_t* data, size_t size, bool more) const {
  assert(underpants);
  underpants->sendRaw(data, size, more);
}

void ModuleBase::addStateCallback(ApplicationState state, function<void(void)> callback) {
  assert(underpants);
  underpants->addStateCallback(state, callback);
}

void ModuleBase::setShutdownRequestHandler(function<bool(void)> handler) {
  assert(underpants);
  underpants->setShutdownRequestHandler(handler);
}

string ModuleBase::createCallbackUID() const {
  assert(underpants);
  return underpants->createCallbackUID();
}

void ModuleBase::subscribe(const char *prefix, size_t len) {
  assert(underpants);
  underpants->subscribe(prefix, len);
}

void ModuleBase::unsubscribe(const char *prefix, size_t len) {
  assert(underpants);
  underpants->unsubscribe(prefix, len);
}

void ModuleBase::setExtraHandler(std::function<void(zmq::pollitem_t)>
                                 handler) {
  assert(underpants);
  underpants->setExtraHandler(handler);
}

void ModuleBase::addPollitem(pollitem_t pi) {
  assert(underpants);
  underpants->moduleThread->addPollitem(pi);
}

void ModuleBase::removePollitem(pollitem_t pi) {
  assert(underpants);
  underpants->moduleThread->removePollitem(pi);
}
