from conans import ConanFile, CMake, tools


class PhgnativebaseplateConan(ConanFile):
    name = "phg-native-baseplate"
    version = "0.1.4"
    license = "Apache-2.0"
    author = "Jacob Andersen <jacob.andersen@alexandra.dk>"
    url = "https://bitbucket.org/4s/phg-native-baseplate"
    description = "The baseplate for 4S PHG Core native modules."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": True, "protobuf:shared": True,
                       "protobuf:with_zlib": False, "protobuf:fPIC": True, "protobuf:lite": True,
                       "zmq:shared": True, "zmq:fPIC": True, "zmq:encryption": None}
    generators = "cmake"
    exports_sources = "src/*", "include/*", "CMakeLists.txt", "test/*"
    requires = "protobuf/3.9.1@_/_", "cppzmq/4.4.1@bincrafters/stable", "zmq/4.3.2@bincrafters/stable", "phg-messages/0.2.5@_/_"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        if tools.get_env("CONAN_RUN_TESTS", True):
            cmake.test()

    def build_requirements(self):
        if tools.get_env("CONAN_RUN_TESTS", True):
            self.build_requires("gtest/1.10.0")

    def package(self):
        self.copy("*", dst="include", src="include")
        self.copy("*messages.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["native_baseplate"]
