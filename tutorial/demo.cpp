/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief Minimal BasePlate demo.
 *
 * This tiny application is intended to demonstrate the use of the
 * native base-plate through a minimal example involving only two
 * modules and a single interface. The demonstraion covers:
 *  - How to define an interface.
 *  - How to transform an interface into a Connector subclass,
 *    creating the outlet and plug Connector subclasses.
 *  - How to create a module (two, actually) that will:
 *    - Collaborate on solving a problem using the defined interface.
 *    - Shutdown in a coordinated way when the job is complete.
 *    - Use logging.
 *  - How to create a module to handle the logging output.
 *  - How to create a full application that will:
 *    - Start a number of modules.
 *    - Wait until they are done before terminating.
 *
 * The main() function creates a log output module and two "regular"
 * modules implementing each end (plug and outlet) of an interface
 * defined by the PrimeConnector class. The modules then collaborates
 * on finding the first 10 prime numbers, and then exits. The main()
 * function will wait until the modules have completed their work.
 *
 * The first module, CounterModule, implements the PrimePlug. This
 * module will start counting from 1 and iterate every 200 ms. The
 * numbers are sent to the CalculatorModule, which implements the
 * PrimeOutlet. The CalculatorModule receives a number, tests for
 * primality, and returns the answer to CounterModule, which will then
 * print the result to the log as an "info" message.
 *
 * @todo Write more about how the application will terminate.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#include "BasePlate.hpp"

#include <iostream>
#include <memory>
#include <vector>
#include "TutorialMessage.pb.h"

namespace s4 {
  namespace BasePlate {
    namespace Demo {
      
      /**********************************************************************/
      /*                                                                    */
      /*                         Application modules                        */
      /*                                                                    */
      /**********************************************************************/
      
      
      /**
       * @brief Iterate through natural numbers and log the calculation
       *        result for each value.
       */
      class CounterModule : public ModuleBase {
        
        uint32_t primeCount = 0;
        
      public:
        void testNumber(uint32_t n) {
          TutorialMessage m;
          m.mutable_number()->set_number(n);
          sendMulticastWithCallback<s4::BasePlate::TutorialMessage>("IsPrime",  [this, n](s4::BasePlate::TutorialMessage* m) -> void {
            if (n == 50) {
              requestShutdown();
              return;
            }
            
            if (m->verifiednumber().isprime()) {
              // If the number is prime, we report it to the logger
              primeCount++;
              info("Found prime number " + std::to_string(primeCount) + ": " +
                  std::to_string(m->verifiednumber().number()));
            }
            
          }, [](int, string msg) -> void {
            // On error
          }, &m);
        }
        
        CounterModule(Context* context, peer_t id) : ModuleBase(context, id) {
          // When the application is running, we test the first number
          addStateCallback(RUNNING, [this]() -> void {
            dbg("HEY - we are starting the counter!");
            for (int i = 2; i <= 50; i++) {
              testNumber(i);
            }
          });
          start();
        }
      };
      
      
      /**
       * @brief Calculate whether a given value is a prime.
       */
      class CalculatorModule : public ModuleBase {
        
      public:
        CalculatorModule(Context* context, peer_t id) : ModuleBase(context, id) {
          addFunction<TutorialMessage>("IsPrime", [this](MetaData metadata,
                                                         TutorialMessage* n,
                                                         Callback respond,
                                                         ErrorCallback error) -> void {
            
            uint32_t number = n->number().number();
            
            dbg("Testing " + std::to_string(number));
            
            // Test if the given number is a prime
            bool isPrime = testNumber(number);
            
            // Respond to the sender
            TutorialMessage m;
            m.mutable_verifiednumber()->set_number(number);
            m.mutable_verifiednumber()->set_isprime(isPrime);
            respond(&m);
          });
          
          start();
        }
        
        /**
         * Test if the given number is a prime.
         *
         * @param number A positive integer.
         * @return True iff the number is prime.
         */
        bool testNumber(uint32_t number) {
          uint32_t i;
          bool isPrime = true;
          for (i = 2; i <= sqrt(number); i++) {
            if (number % i == 0) {
              isPrime = false;
              break;
            }
          }
          return isPrime;
        }
        
      };
      
      
      /**********************************************************************/
      /*                                                                    */
      /*                                Main                                */
      /*                                                                    */
      /**********************************************************************/
      
      
      /**
       * @brief The demo application.
       *
       * The application spins up two modules plus a third logger module,
       * which will print the logging output of the first two modules to
       * \c stderr.
       *
       * The two application modules will collaborate on finding the prime
       * numbers < 40 and then self terminate. Notice how the
       * execution of the demo() function is blocked until the modules
       * finish their work.
       */
      int demo() {
        
        // Define zmq sockets and name of the Master module
        BasePlate baseplate("inproc://module2reflector", "inproc://reflector2module", "Master");
        
        baseplate.run({new CounterModule(&baseplate.context, "Counter"),
          new CalculatorModule(&baseplate.context, "Calculator")}, 0, LogMessage_Level_DBG);
        
        return 0;
      }
    }
  }
}

// This will be hidden from the docs to avoid clutter
#ifndef __DOXYGEN__
// Redirect a global main function to the one found in the Demo
// namespace.
int main(void) {
  return s4::BasePlate::Demo::demo();
}
#endif
