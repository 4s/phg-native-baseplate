/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an "AS
 *   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 *   express or implied. See the License for the specific language
 *   governing permissions and limitations under the License.
 */

/**
 * @file
 * @brief The Master Module of 4S PHG C++ 'baseplate' subsystem
 * implemented using ZeroMQ.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 * Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef MASTERMODULE_HPP
#define MASTERMODULE_HPP

#include "Common.hpp"
#include "ModuleBase.hpp"
#include <map>
#include <set>

namespace s4 {
  namespace BasePlate {    
    
    /**
     * @brief This module orchestartes startup and shutdown of the application.
     */
    class MasterModule : public ModuleBase {
      
    public:
      MasterModule(Context& context,
                   int modules);
      void addStateCallback(ApplicationState state,
                            std::function<void()> callback);
      void requestShutdown(bool force = false) const;
    private:
      void start();
      bool all(bool);
      std::map<peer_t, bool> running;
      int modules;
    };
    
  }
}

#endif /* MASTERMODULE_HPP */
