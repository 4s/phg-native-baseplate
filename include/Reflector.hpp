/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief The reflector in the 4S PHG C++ 'baseplate' subsystem.
 *
 * The reflector is implemented using ZMQ, where it is usually known as
 * a 'Forwarder'.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *  Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef REFLECTOR_HPP
#define REFLECTOR_HPP

#include <functional>
#include "Common.hpp"

namespace s4 {
  namespace BasePlate {
    
    /**
     * @brief The actual "base plate" -- the foundation linking
     *        modules together.
     */    
    class Reflector {
    public:
      /**
       * @brief Constructs a single stand-alone Reflector.
       *
       * For whenever all modules of an application will be running atop
       * one single Reflector.
       */
      
      Reflector(Context* context);
      
      /**
       * @brief Destructor
       *
       * On a threaded base plate implementation, the Reflector destructor
       * will block until all modules have terminated, so make sure to
       * wait for the callback from setShutdownCompleteCallback() before
       * destroying the Reflector if you do not want your thread to block.
       *
       * On a non-threaded base plate implementation, the behaviour is
       * undefined if the Reflector is destroyed before the modules have
       * terminated properly. The implementation may brutally kill all
       * modules, throw an exception (from the destructor, which may
       * also cause undefined behaviour!), or completely terminate the
       * application.
       */

      ~Reflector();
      
      /**
       * @brief Start the distributed application.
       *
       * This function must be called when all modules for the
       * application have been constructed in order to start executing
       * the distributed application. The function must be called
       * exactly once to start the application. Repeated invocations
       * will result in an std::runtime_error exception.
       *
       * The function hands over module management from the caller
       * thread to the internal management thread of the Reflector. The
       * caller must therefore ensure that all modules have been fully
       * constructed, otherwise the outcome is undefined.
       */
      
      void start(void);
      
    private:

      /// @brief Opaque wrapper of the private parts.
      class Underpants;
      
      /// @brief Container of the private parts.
      Underpants *underpants;      
    };
   
  }
}

#endif /* REFLECTOR_HPP */
