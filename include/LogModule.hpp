/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Definition of abstract Log module for 4s BasePlate system.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas Lindstrøm</a>,
 * The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef LOGMODULE_HPP
#define LOGMODULE_HPP

#include "ModuleBase.hpp"


namespace s4 {
  namespace BasePlate {
    
    class AbstractLogModule : public virtual ModuleBase {
    protected:
      
      /**
       * @brief Virtual base module for logger modules.
       *
       * @param level The logging level, eg. the lowest level the logger should
       * report. The order is DBG, INFO, WARNING, ERROR, FATAL.
       */
      AbstractLogModule(s4::messages::classes::log::LogLevel level);
      
    private:
      virtual void fatal(peer_t moduleId, std::string message, std::string file, int line) = 0;
      virtual void error(peer_t moduleId, std::string message, std::string file, int line) = 0;
      virtual void warning(peer_t moduleId, std::string message, std::string file, int line) = 0;
      virtual void information(peer_t moduleId, std::string message, std::string file, int line) = 0;
      virtual void debug(peer_t moduleId, std::string message, std::string file, int line) = 0;
      
    protected:
      s4::messages::classes::log::LogLevel level;
      void start();
    };
  }
}

#endif
