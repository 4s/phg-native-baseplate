/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief Functions shared by classes in the 4S PHG C++ 'baseplate' subsystem.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *  Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef COMMON_HPP
#define COMMON_HPP

#include <zmq.hpp>
#include <string>

namespace s4 {
  namespace BasePlate {
    /**********************************************************************/
    /*                                                                    */
    /*                         Helper functions                           */
    /*                                                                    */
    /**********************************************************************/
    
    inline std::string message2string(const zmq::message_t &msg) {
      return std::string(msg.data<char>(), msg.size());
    }
    
    inline zmq::message_t string2message(const std::string &msg) {
      return zmq::message_t(msg.c_str(), msg.length());
    }
    
    enum ApplicationState {
      /// Init
      INITIALIZING,
      /// Rdy
      READY,
      RUNNING,
      SHUTDOWN_REQUEST,
      FINALIZING
    };
    
    // This represents the id of the modules
    typedef std::string peer_t;
    
    enum MessageType {
      UNICAST,
      MULTICAST,
      BROADCAST
    };
    
    class Context {
    public:
      Context(std::string module2Reflector = "inproc://module2reflector",
              std::string reflector2Module = "inproc://reflector2module",
              std::string masterId = "Master")
      : masterId(masterId), module2Reflector(module2Reflector), reflector2Module(reflector2Module), context(1) {};
      
      std::string masterId;
      std::string module2Reflector;
      std::string reflector2Module;
      zmq::context_t context;
    };
  }
}

#endif /* COMMON_HPP */
