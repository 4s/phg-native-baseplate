/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an "AS
 *   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 *   express or implied. See the License for the specific language
 *   governing permissions and limitations under the License.
 */

/**
 * @file
 * @brief Definitions of the Function class which all functions that modules
 * expose to each other should implement..
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 * Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef DISPATCHERS_HPP
#define DISPATCHERS_HPP

#include <google/protobuf/message_lite.h>
#include <functional>
#include <string>
#include "Common.hpp"


using server_callback = std::function<void(google::protobuf::MessageLite*, bool)>;

using error_callback = std::function<void(int, std::string)>;

namespace s4 {
  namespace BasePlate {
    
    /**
     * @brief The meta data sent with all function calls.
     */
    struct MetaData {
      s4::BasePlate::MessageType messageType;
      std::string signal;
      s4::BasePlate::peer_t sender;
      std::string callback; // Added temporarily to help support the transition to the new baseplate
    };
    
    /**
     * @brief A callback method that is given to functions. This callback may be kept alive.
     */
    class Callback {
      
    public:
      Callback(server_callback callback) : callback(callback) {};
      virtual~ Callback() {};
      
      Callback& keepAlive() {
        keep = true;
        return *this;
      }
      
      Callback& last() {
        keep = false;
        return *this;
      }

      void operator() (google::protobuf::MessageLite* message) const {
        callback(message, keep);
      }
      
    private:
      server_callback callback;
      bool keep = false;
    };
    

    /**
     * @brief An error callback method. This callback may be kept alive.
     */
    class ErrorCallback {
      
    public:
      ErrorCallback(std::function<void(int, std::string, bool)> callback) : callback(callback) {};
      virtual~ ErrorCallback() {};
      
      ErrorCallback& keepAlive() {
        keep = true;
        return *this;
      }
      
      ErrorCallback& last() {
        keep = false;
        return *this;
      }

      void operator() (int code, std::string msg) const {
        callback(code, msg, keep);
      }
      
    private:
      std::function<void(int, std::string, bool)> callback;
      bool keep = false;
    };
    
    /**
     * @brief Abstract base class for all module functions.
     *
     * Input and output must be subclasses of google::protobuf::MessageLite.
     */
    class Function {
    protected:
      Function(bool expectsData) : takesInput(expectsData) { };
      
    public:
      virtual ~Function() { };
      
      /**
       * @return True iff the function expects input data.
       */
      bool expectsData() const {
        return takesInput;
      };
      
      /**
       * @brief Execute this function.
       *
       * @param metadata The meta data of this call
       * @param input The input data if this is expected (if expectsData() == true) or a nullptr.
       * @param callback Callback function for responding to the sender.
       * @param error Error callback function. Notifies the sender that an error has occured.
       */
      virtual void apply(MetaData& metadata,
                         google::protobuf::MessageLite* input,
                         Callback& callback,
                         ErrorCallback& error) = 0;
      
      /**
       * @return A new instance of the input type if input is expected.
       * Must be deleted by the caller.
       */
      virtual google::protobuf::MessageLite* getInstance() { return nullptr; };
      
    private:
      bool takesInput;
    };
    
    /**
     * @brief A module function which does not take input data.
     */
    class Procedure : public Function {
    public:
      /**
       * @brief Create a new procedure (no input) with a callback function.
       */
      Procedure(std::function<void(MetaData&, Callback&, ErrorCallback&)> function) : Function(false), function(function) {};
      
      /**
       * @brief Create a new procedure (no input) without a callback function.
       */
      Procedure(std::function<void(MetaData&)> noCallbackFunction) : Function(false),
      function([noCallbackFunction](MetaData& metadata,
                                    Callback&,
                                    ErrorCallback& error) -> void { noCallbackFunction(metadata); }) {};
      
      void apply(MetaData& metadata, google::protobuf::MessageLite*,
                 Callback& callback,
                 ErrorCallback& error) override {
        function(metadata, callback, error);
      };
      
    private:
      std::function<void(MetaData&, Callback&, ErrorCallback&)> function;
    };
    
    /**
     * @brief A module function which takes input data.
     */
    template <class A>
    class Consumer : public Function {
    public:
      /**
       * @brief Create a new consumer (takes input of type A) with a callback function.
       */
      Consumer(std::function<void(MetaData&, A*,
                                  Callback&,
                                  ErrorCallback&)> function) : Function(true), function(function) {};
      
      /**
       * @brief Create a new consumer without callback function.
       */
      Consumer(std::function<void(MetaData&, A*)> noCallbackFunction)
      : Consumer([noCallbackFunction](MetaData& metadata,
                                      A* input,
                                      Callback&,
                                      ErrorCallback& error) -> void { noCallbackFunction(metadata, input); }) {};
      
      void apply(MetaData& metadata,
                 google::protobuf::MessageLite* input,
                 Callback& callback,
                 ErrorCallback& error) override {
        function(metadata, (A*) input, callback, error);
      };
      
      google::protobuf::MessageLite* getInstance() override {
        return new A();
      }
      
    private:
      std::function<void(MetaData&, A*, Callback&, ErrorCallback&)> function;
    };
  }
}

#endif
