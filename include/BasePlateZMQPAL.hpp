/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief The 4S PHG C++ 'baseplate' platform abstraction subsystem
 *        for ZeroMQ-based baseplates.
 *
 * This header defines the \c s4::BasePlate::PAL interface between
 * the common PHG platform independent C++ baseplate implementation
 * and the platform specific elements.
 *
 * The baseplate implementation using this particular header, will
 * also depend on ZeroMQ, which in turn (at least in its standard
 * implementation) depends on POSIX threads.
 *
 * @todo Consider generalising this interface in such a way so that it
 * doesn't depend on ZeroMQ. That would perhaps be a more elegant
 * solution. Currently, it depends on the \c zmq::pollitem_t and
 * \c zmq::error_t types defined by ZeroMQ
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef BASEPLATE_ZMQ_PAL_HPP
#define BASEPLATE_ZMQ_PAL_HPP

#include <functional>
#include <zmq.hpp>

namespace s4 {
  namespace BasePlate {
    namespace PAL {
      
      /**
       * @brief The threading model of a module.
       *
       * Modules may execute on individual threads; a single common "main"
       * thread; or a pool of threads. Modules are \b not thread safe,
       * hence, in the latter case each module requires that only a single
       * thread will be executing its code at any point in time (with
       * proper memory barriers if multiple threads may execute its
       * code). This may for instance be realised using the [Grand Central
       * Dispatch](https://en.wikipedia.org/wiki/Grand_Central_Dispatch)
       * "serial queues".
       *
       * The ModuleThreader is an abstract base class for the thread of a
       * module. The thread is started using start() and joined using
       * join() as one would expect. The thread must be joined by the
       * derived class before it is destructed (typically in its
       * destructor) as the thread depends on the derived object's
       * existence.
       *
       * The derived object implements the init() and handler() functions
       * which is called by the thread. The thread exits when no more
       * pollitem handlers are registered and one of the handler() (or
       * init()) functions returns a value \c <0.
       */
      
      class ModuleThreader {
      public:
        
        ModuleThreader();
        
        
        /**
         * Destructor
         *
         * Destructing a not-yet-joined ModuleThreader is not allowed and
         * will trigger an assertion. The derived class *must* make sure
         * that the thread is joined before its destruction -- otherwise
         * the virtual handler functions (which the thread depends on)
         * will dissappear while the thread is still active.
         */
        
        virtual ~ModuleThreader();
        
      protected:
        
        
        /**
         * Thread initialiser.
         *
         * The entry point of the newly started thread. Called as the
         * first task of the thread after it has been started (with
         * start()). It is typically the task of the initialiser to create
         * the necessary communication sockets and register relevant
         * pollitems.
         *
         * @return The timeout in milliseconds, with 0 meaning a handler()
         *         must be invoked as soon as possible, and <0 meaning
         *         wait indefinitely (until something happens on a
         *         registered pollitem).
         */
       
        virtual long init(void) noexcept =0;
        
        
        /**
         * Event handler.
         *
         * Handle a pollitem event. Note that if multiple events are
         * registered in the pollevent (e.g. both of the \c ZMQ_POLLIN and
         * \c ZMQ_POLLOUT events), more than one event could be flagged in
         * the \c event.revents.
         *
         * @param event The pollitem event to handle. The actual event
         *              (possibly more than one) is in the \c event.revents
         *              bitmask.
         * @return The timeout in milliseconds, with 0 meaning a handler()
         *         must be invoked as soon as possible, and <0 meaning
         *         wait indefinitely (until something happens on a
         *         registered pollitem). Note that multiple handlers may
         *         request different timeouts, and the shortest will
         *         always be selected.
         */
        
        virtual long handler(zmq::pollitem_t event) noexcept =0;
        
        
        /**
         * Timeout handler.
         *
         * Handle a timeout situation. This is only supposed to happen if
         * a previous handler() or init() invocation has returned a
         * non-negative value. The standard behaviour (if not overridden)
         * is to just return \c -1.
         *
         * @return The timeout in milliseconds, with 0 meaning a handler()
         *         must be invoked as soon as possible, and <0 meaning
         *         wait indefinitely (until something happens on a
         *         registered pollitem). Note that multiple handlers may
         *         request different timeouts, and the shortest will
         *         always be selected.
         */
        
        virtual long handler(void) noexcept {return -1;}
        
        
        /**
         * Exception handler.
         *
         * Handle an exception during polling. E.g. due to an
         * interrupt. In case of an exception during polling, all
         * registered pollitems will be removed (before the handler is
         * called). So the handler may simply return \c -1 to exit the
         * thread. This is the standard behaviour if the function is not
         * overridden.
         *
         * @param error The exception generated by the poller.
         * @return The timeout in milliseconds, with 0 meaning a handler()
         *         must be invoked as soon as possible, and <0 meaning
         *         wait indefinitely (until something happens on a
         *         registered pollitem). Note that multiple handlers may
         *         request different timeouts, and the shortest will
         *         always be selected.
         */
        
        virtual long handler(zmq::error_t error) noexcept {return -1;}
        
        
        /**
         * Start the thread.
         *
         * Called by the creator of the thread to start it. Shall only be
         * called once. If the thread is already started, this will result
         * in a \c std::runtime_error.
         */
        
        void start(void);
        
        
        /**
         * Join with the thread.
         *
         * Called by the creator of the thread (typically) to synchronize
         * with its termination. Shall only be called once. If the thread
         * is already joined, this will result in a \c
         * std::runtime_error. Of course it is illegal to attempt to join
         * with a thread from itself.
         */
        
        void join(void);
        
        
        /**
         * Insert pollitem.
         *
         * Adds pollitem events to the list of events we subscribe
         * to. Note that the events mask is OR'ed into the already
         * registered events on the same socket.
         *
         * @param pollitem The pollitem. \c pollitem.events is
         *                 OR'ed onto the existing bitmask for
         *                 the same socket or file descriptor.
         */
        
        void addPollitem(zmq::pollitem_t pollitem);
        
        
        /**
         * Removes pollitem.
         *
         * Removes pollitem events from the list of events we subscribe
         * to. Note that the events mask is AND'ed with already registered
         * events on the same socket.
         *
         * @param pollitem The pollitem. \c pollitem.events is
         *                 AND'ed out of the existing bitmask for
         *                 the same socket or file descriptor.
         */
        
        void removePollitem(zmq::pollitem_t pollitem);
        
        
        /**
         * Get a list of pollitems.
         *
         * Returns the current list of registered pollitems.
         *
         * @param items Pointer which the function directs to the current
         *              list of registered pollitems. The list array
         *              pointed to when the function returns is owned by
         *              the callee, and must not be altered by the caller.
         *              The lifetime of the array pointed to by \c items
         *              is limited, so the array is invalidated if another
         *              call is made to the ModuleThreader. Therefore, all
         *              necessary information must be extracted from this
         *              list immediately (or the entire list must be
         *              copied).
         * @return The number of elements in the \c items list.
         */
        
        size_t getPollitems(zmq::pollitem_t const **items);

        void setFinalCallback(std::function<void(void)> function);

      private:
        /// @brief Opaque wrapper of the private parts.
        class Underpants;
        
        /// @brief Container of the private parts.
        Underpants *underpants;
      };
    }
  }
}

#endif // BASEPLATE_ZMQ_PAL_HPP
