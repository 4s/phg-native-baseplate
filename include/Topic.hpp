/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an "AS
 *   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 *   express or implied. See the License for the specific language
 *   governing permissions and limitations under the License.
 */

/**
 * @file
 * @brief Topics for messages sent over ZeroMQ int the 4S PHG C++ 'baseplate' subsystem.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef TOPIC_HPP
#define TOPIC_HPP

#include <string>
#include "Common.hpp"
using std::string;

namespace s4 {
  namespace BasePlate {
    
    inline peer_t nullID() {
      return peer_t("");
    }
    
    class Topic {
      
    public:
      static Topic unicast(peer_t destination, string function, string signal, peer_t sender, string callback = "");
      static Topic broadcast(string function, string signal);
      static Topic multicast(string function, string signal, peer_t sender, string callback = "");
      static Topic subscribeUnicast(peer_t destination);
      static Topic subscribeMulticast(string function);
      static Topic subscribeBroadcast();
      static Topic fromString(string topic);
      
      /**
       * Return a string representation of this topic.
       */
      std::string asString();
      
      MessageType type;
      peer_t destination;
      string function;
      string signal;
      peer_t sender;
      string callback;
      
    private:
      Topic(MessageType type,
            peer_t destination,
            string function,
            string signal,
            peer_t sender,
            string callback)
      : type(type), destination(destination), function(function), signal(signal), sender(sender), callback(callback) {};
      
      void parseString(string topic);
      peer_t parseId(string s);
      string stringifyId(peer_t id);
      char seperator = '_';
      
    };
  }
}

#endif /* TOPIC_HPP */
