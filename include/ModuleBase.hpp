/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief Base class for modules in the 4S PHG C++ 'baseplate' subsystem.
 * All modules should inherit from this class.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *  Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef MODULEBASE_HPP
#define MODULEBASE_HPP

#include <google/protobuf/message_lite.h>
#include <functional>
#include <string>

#include "Topic.hpp"
#include "Function.hpp"
#include "Common.hpp"
#include "Log.pb.h"
#include "Error.pb.h"

#define dbg(message) log(s4::messages::classes::log::DEBUG, message, __FILE__, __LINE__);
#define info(message) log(s4::messages::classes::log::INFO, message, __FILE__, __LINE__);
#define warn(message) log(s4::messages::classes::log::WARN, message, __FILE__, __LINE__);
#define err(message) log(s4::messages::classes::log::ERROR, message, __FILE__, __LINE__);
#define ftl(message) log(s4::messages::classes::log::FATAL, message, __FILE__, __LINE__);

namespace s4 {
  namespace BasePlate {
    
    /**
     * @brief Abstract base class for all PHG modules.
     *
     */
    class ModuleBase {

    public:
      // We have to allow destruction of a module using its
      // ModuleBase pointer.  For instance an application could create
      // all its modules and store the pointers in a collection
      // (e.g. std::vector<ModuleBase*>) for later deletion.
      virtual ~ModuleBase(void);
      
      /**
       * Get the name of this module
       */
      peer_t getId() const;

    protected:
      ModuleBase(Context& context, peer_t id);
      
      /**
       * Start the module. This function must be called exactly once as the last
       * action by the most derived constructor.
       */
      void start();
      
      /**
       * Add callback when the application state changes
       *
       * @param state When the application state changes to this state, the callback is called.
       * @param callback The callback function.
       */
      void addStateCallback(ApplicationState state,
                            std::function<void()> callback);
      
      /**
       * Define a handler to respond to shutdown requests. The handler should return either
       *  - TRUE: If the module accepts the shutdown request.
       *  - FALSE: If the module cancels the shutdown procedure.
       *
       * The default handler always accepts requests.
       */
      void setShutdownRequestHandler(std::function<bool(void)> handler);
      
      void setFinalCallback(std::function<void(void)> function);
      
      /**
       * Helper function for addFunction(std::string name, Function* function). This method
       * adds a Consumer function which expects data of type A. A must be a subclass of
       * google::protobuf::MessageLite.
       *
       * @param name The name of the function, this callback
       *                  handles. Only one callback per function can
       *                  be registered.
       * @param function The callback function.
       */
      template <class A>
      void addFunction(std::string name,
                       std::function<void(MetaData, A*)> function) {
        addFunction(name, new Consumer<A>(function));
      }
      
      /**
       * Helper function for addFunction(std::string name, Function* function). This method
       * adds a Consumer function which expects data of type A and gives a callback. A
       * must be a subclass of google::protobuf::MessageLite.
       *
       * @param name The name of the function, this callback
       *                  handles. Only one callback per function can
       *                  be registered.
       * @param function The callback function.
       */
      template <class A>
      void addFunction(std::string name,
                       std::function<void(MetaData, A*, Callback&, ErrorCallback&)> function) {
        addFunction(name, new Consumer<A>(function));
      }
      
      /**
       * Helper function for addFunction(std::string name, Function* function). This method
       * adds a Procedure which does not expects input data.
       *
       * @param name The name of the function, this callback
       *                  handles. Only one callback per function can
       *                  be registered.
       * @param function The callback function.
       */
      void addFunction(std::string name,
                       std::function<void(MetaData)> function) {
        addFunction(name, new Procedure(function));
      }

      /**
       * Helper function for addFunction(std::string name, Function* function). This method
       * adds a Procedure which does not expects input data but gives a callback.
       *
       * @param name The name of the function, this callback
       *                  handles. Only one callback per function can
       *                  be registered.
       * @param function The callback function.
       */
      void addFunction(std::string name,
                       std::function<void(MetaData, Callback&, ErrorCallback&)> function) {
        addFunction(name, new Procedure(function));
      }
      
      /**
       * Set a handler which will subscribe to anything that the reflector (that this module
       * is connected to) publishes.
       * 
       * @param function The default handler.
       */
      void setDefaultFunction(std::function<void(zmq::message_t&)> function);

      
      
      /**
       * Remove a function previously added using the addFunction method.
       *
       * @param interface The name of the interface.
       */
      void removeFunction(std::string interface);
      
      /**
       * Sends a shutdown request to the other modules. If all accept the request,
       * the application proceeds to the FINALIZING state and shuts down.
       */
      void requestShutdown(bool force = false) const;

    public:
      
      /**
       * Send a message to another module.
       *
       * @param destination The id of the receiving module.
       * @param interface The interface on the receiving module that must handle this
       *                  message.
       * @param signal An optional parameter that the receiving module will receive.
       * @param data A pointer to an optional google protocol buffers message to
       *             be sent along with the message. The receiver must know what
       *             exact type to expect.
       */
      void sendUnicast(peer_t destination,
                       std::string interface,
                       std::string signal,
                       google::protobuf::MessageLite* data = nullptr) const;

      /**
       * Send a message to another module.
       *
       * @param destination The id of the receiving module.
       * @param interface The interface on the receiving module that must handle this
       *                  message.
       * @param data A pointer to an optional google protocol buffers message to
       *             be sent along with the message. The receiver must know what
       *             exact type to expect.
       */
      void sendUnicast(peer_t destination,
                       std::string interface,
                       google::protobuf::MessageLite* data = nullptr) const {
        sendUnicast(destination, interface, "N/A", data);
      }

      /**
       * Send a message to another module.
       *
       * @param destination The id of the receiving module.
       * @param interface The interface on the receiving module that must handle this
       *                  message.
       * @param signal An extra parameter that the receiving module will receive.
       * @param callback The lambda that will be called when another module responds
       *                 to this request.
       * @param data A pointer to an optional google protocol buffers message to
       *             be sent along with the message. The receiver must know what
       *             exact type to expect.
       */
      template <class A>
      void sendUnicastWithCallback(peer_t destination,
                                   std::string interface,
                                   std::string signal,
                                   std::function<void(A*)> callback,
                                   error_callback error,
                                   google::protobuf::MessageLite* data = nullptr) {
        std::string uid = addCallbacks(callback, error);
        sendUnicastWithCallback(destination, interface, signal, uid, data);
      }

      template <class A>
      void sendUnicastWithCallback(peer_t destination,
                                   std::string interface,
                                   std::function<void(A*)> callback,
                                   error_callback error,
                                   google::protobuf::MessageLite* data = nullptr) {
        sendUnicastWithCallback(destination, interface, "N/A", callback, error, data);
      }
      
      /**
       * Send a message all other modules which has a dispatcher listening on the given
       * interface.
       *
       * @param interface The interface on the receiving module that must handle this
       *                  message.
       * @param signal An optional parameter that the receiving module will receive.
       * @param data A pointer to an optional google protocol buffers message to
       *             be sent along with the message. The receiver must know what
       *             exact type to expect.
       */
      void sendMulticast(std::string interface,
                         std::string signal,
                         google::protobuf::MessageLite* data = nullptr) const;

      /**
       * Send a message all other modules which has a dispatcher listening on the given
       * interface.
       *
       * @param interface The interface on the receiving module that must handle this
       *                  message.
       * @param data A pointer to an optional google protocol buffers message to
       *             be sent along with the message. The receiver must know what
       *             exact type to expect.
       */
      void sendMulticast(std::string interface,
                         google::protobuf::MessageLite* data = nullptr) const {
        sendMulticast(interface, "N/A", data);
      }
      
      /**
       * Send a multicast and register a callback lambda which is called when the
       * receiver responds to the request.
       * @param interface The interface on the receiving module that must handle this
       *                  message.
       * @param signal An optional parameter that the receiving module will receive.
       * @param callback The lambda that will be called when another module responds
       *                 to this request.
       * @param data A pointer to an optional google protocol buffers message to
       *             be sent along with the message. The receiver must know what
       *             exact type to expect.
       */
      template <class A>
      void sendMulticastWithCallback(std::string interface,
                                     std::string signal,
                                     std::function<void(A*)> callback,
                                     error_callback error,
                                     google::protobuf::MessageLite* data = nullptr) {
        std::string uid = addCallbacks(callback, error);
        sendMulticastWithCallback(interface, signal, uid, data);
      }

      template <class A>
      void sendMulticastWithCallback(string interface,
                                     std::function<void(A*)> callback,
                                     error_callback error,
                                     google::protobuf::MessageLite* data = nullptr) {
        sendMulticastWithCallback(interface, "N/A", callback, error, data);
      }
      
      /**
       * Send a message all other modules which has a dispatcher listening on the given
       * interface.
       *
       * @param interface The interface on the receiving module that must handle this
       *                  message.
       * @param signal An extra parameter that the receiving module will receive.
       * @param data A pointer to an optional google protocol buffers message to
       *                be sent along with the message. The receiver must know what
       *                exact type to expect.
       */
      void sendBroadcast(std::string interface,
                         std::string signal,
                         google::protobuf::MessageLite* data = nullptr) const;
      void sendBroadcast(std::string interface,
                         google::protobuf::MessageLite* data = nullptr) const {
        sendBroadcast(interface, "N/A", data);
      }
      
      /**
       * Send a part of a multi-part message directly to the reflector.
       *
       * @param data The data to send.
       * @param more True iff there are more parts of this multi-part message.
       */
      void sendRaw(std::string data, bool more) const;
      void sendRaw(std::vector<int8_t> data, bool more) const;
      void sendRaw(int8_t* data, size_t size, bool more) const;

      /**
       * Get the current application state.
       */
      ApplicationState getApplicationState();
      
    protected:
      /**
       * @brief Append a message to the log with the given level and message.
       *
       * Append a message to the log with the given level and message. This should not be used directly.
       * Instead the macros, dbg, info, warn, err and ftl should be used.
       */
      void log(s4::messages::classes::log::LogLevel level,
               std::string msg,
               std::string file,
               int line) const noexcept;
      
      /**
       * Add callback method to this module. Should be called before a message requesting a
       * callback is sent to another module. The callback method is removed after it has been
       * called the first time.
       *
       * @param callback The callback method to be added.
       * @param error The error callback method to be added.
       * @returns The name of the callback method. Should be sent as the 'callback' param in
       * the message topic.
       */
      template <class A = google::protobuf::MessageLite>
      std::string addCallbacks(std::function<void(A*)> callback, error_callback error) {
        std::string uid = createCallbackUID();

        // Add callback on the UID generated above
        addFunction<A>(uid, [this, uid, callback] (MetaData metadata,
                                                   A* msg) -> void {
          callback(msg);
          
          // We remove the ERR callback first since removing the actual callback will
          // cause this lambda expression to be deleted.
          if (metadata.signal != "KEEP") {
            removeFunction("ERR" + uid);
            removeFunction(uid);
          }
        });
        
        // Add error callback with same UID with 'ERR' prefix
        addFunction<s4::messages::classes::error::Error>
          ("ERR" + uid, [this, uid, error] (MetaData metadata, 
                      s4::messages::classes::error::Error* e) -> void {
          error(e->error_code(), e->message());
          
          // We remove the callback first since removing the ERR callback will
          // cause this lambda expression to be deleted.
          if (metadata.signal != "KEEP") {
            removeFunction(uid);
            removeFunction("ERR" + uid);
          }
        });
        
        return uid;
      }
      
    protected:
      /**
       * Assign a callback function to a key (name). All broadcasts, multicasts and
       * unicasts to this module with the function parameter equal to this name will invoke
       * the callback. Should not be used directly - use the two public addFunction methods
       * instead.
       *
       * @param name The name of the function, this callback
       *             handles. Only one callback per function can
       *             be registered.
       * @param function The callback function. Must be either a Consumer, if the
       *                 function expects data, or a Procedure if it does not expect data.
       */
      void addFunction(std::string name, Function* function);

      void sendMulticastWithCallback(std::string interface,
                                     std::string signal,
                                     std::string callbackInterface,
                                     google::protobuf::MessageLite* arg = nullptr) const;
    
      void sendUnicastWithCallback(peer_t destination,
                                   std::string interface,
                                   std::string signal,
                                   std::string callbackInterface,
                                   google::protobuf::MessageLite* arg = nullptr) const;
    private:
      std::string createCallbackUID() const;


      /// @brief Opaque wrapper of the private parts.
      class Underpants;
      
      /// @brief Container of the private parts.
      Underpants *underpants;


      // Following members are only used by the PipingModule
      friend class PipingModule;

      void subscribe(const char *prefix, size_t len);
      void unsubscribe(const char *prefix, size_t len);
      void setExtraHandler(std::function<void(zmq::pollitem_t)> handler);
      void addPollitem(zmq::pollitem_t pi);
      void removePollitem(zmq::pollitem_t pi);

    };    
  }
}

#endif /* MODULEBASE_HPP */
