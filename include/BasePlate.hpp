/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief The 4S PHG C++ 'baseplate' subsystem.
 *
 * All C++ based 4S PHG modules shall provide at least one
 * implementation of the abstract s4::BasePlate::ModuleBase class,
 * which handles the communication with the baseplate subsystem as
 * well as the basis for all the interface connector classes.
 *
 * @todo Complete instruction on baseplate use
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef BASEPLATE_HPP
#define BASEPLATE_HPP

#include "Common.hpp"
#include "ModuleBase.hpp"
#include "Reflector.hpp"
#include "MasterModule.hpp"
#include "PipingModule.hpp"
#include "LogModule.hpp"

#define BOLDRED "\033[1;31m"
#define BOLDYELLOW "\033[1;33m"
#define BOLD "\033[1m"
#define NORMAL "\033[0m"


namespace s4 {
  namespace BasePlate {
    
    /**
     * @brief A simple logger module which prints to stderr.
     *
     * This logger module will print log output to the stderr stream using
     * ANSI escapes for simple colourful layout.
     */

    class ModuleBase;
    class BridgeModule;
    
    class BasePlate {
    public:
      BasePlate(std::string module2Reflector = "inproc://module2reflector",
                std::string reflector2Module = "inproc://reflector2module",
                std::string masterId = "Master") : context(module2Reflector, reflector2Module, masterId) {};
      
      /**
      * Run a baseplate with the given modules.
      * 
      * @param nativeModules Modules running in this process. Note that the modules will be
      *                      destroyed at the end of this method.
      * @param otherModules A list of ID's of other modules that will be running on this baseplate.
      */
      void run(std::set<ModuleBase*> nativeModules = {},
               int otherModules = 0,
               s4::messages::classes::log::LogLevel logLevel = s4::messages::classes::log::DEBUG);
      
      Context context;
    };
    
  }
}


#endif // BASEPLATE_HPP


// TODO: Is the stuff below still relevant?

/*
 
 4SDC message docs below
 
 
 *
 * void msgReady( void )
 * ---------------------
 * This function is called  when the module can start to use the message system.
 * It is guarantied that this function is called before any messages are
 * received by the module.
 *
 * void msgGroupReady( void )
 * --------------------------
 * This function is called when all the static modules in the group are ready
 * to use the message system (this means that the module can interact through
 * the message system with the other static modules in the group).
 *
 * void msgSystemReady( void )
 * ---------------------------
 * This function is called when all static modules in all groups are ready to
 * use the message system (this means that the module can interact through the
 * message system with all other static modules in the system (in all groups))
 *
 * void msgGoingDown( void )
 * -------------------------
 * This function is called to inform the module that the shutdown sequence of
 * the system has been initiated and that it should release all its system
 * resources.
 *
 * void msgDestroy( void )
 * -----------------------
 * After this function is called the module can no longer rely on the message
 * system and should prepare for imminent destruction.
 *
 *******************************************************************************
 *
 * The msgSystemReady() and msgGoingDown() function calls will also be
 * broadcasted as standard messages
 *
 * FSYS::MsgSystemReady
 * --------------------
 * Broadcasted when all static modules are started.  If a module is created
 * after this message is broadcasted, then it won't be able to receive this
 * message, but it will still have its void msgSystemReady( void ) function
 * called.
 *
 * FSYS::MsgGoingDown
 * ------------------
 * Broadcasted when the system is shutting down.  If a module is created after
 * this message is broadcasted, then it won't be able to receive this message,
 * but it will still have its void msgGointDown( void ) function called.
 *
 *******************************************************************************
 *
 * The following messages are being used internally.
 *
 * FSYS::MsgGroupReady
 * -------------------
 * Broadcasted by a group when all of its static modules have launched
 *
 * FSYS::MsgDestroy
 * ----------------
 * Broadcasted when all the threads should terminate
 *
 * FSYS::MsgAllDestroyed
 * ---------------------
 * Broadcasted when all threads are destroyed
 *
 *******************************************************************************
 */

/**
 * @brief Message broadcasted when the system is initialised
 *
 * Broadcasted when all static modules are started.  If a module is created
 * after this message is broadcasted, then it won't be able to receive this
 * message, but it will still have its void msgSystemReady( void ) function
 * called.
 */
//  class MsgSystemReady : public BaseMsg {};

/**
 * @brief Message broadcasted when the system is being terminated
 *
 * Broadcasted when the system is shutting down.  If a module is created after
 * this message is broadcasted, then it won't be able to receive this message,
 * but it will still have its void msgGointDown( void ) function called.
 */
//  class MsgGoingDown : public BaseMsg {};

/**
 * @brief Message broadcasted when all static modules in a group are ready
 *
 * This message is sent by each of the runners when their static modules
 * have started and they are about to enter their message loop
 */
//  class MsgGroupReady : public BaseMsg {};

/**
 * @brief Last message broadcasted to threads
 *
 * After this message has been received the message loops in the system will
 * terminate and the program shut down
 */
//  class MsgDestroy : public BaseMsg {};

/**
 * @brief Last message broadcasted in system
 *
 * When this message is broadcasted, the main threads message loop will
 * terminate.
 */
//  class MsgAllDestroyed : public BaseMsg {};

