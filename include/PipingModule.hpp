/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an "AS
 *   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 *   express or implied. See the License for the specific language
 *   governing permissions and limitations under the License.
 */

/**
 * @file
 * @brief The Bridge Module of 4S PHG C++ 'baseplate' 
 * 
 * A PipingModule is a "normal" module attached to the master
 * baseplate, acting as a proxy for all modules attached to a slave
 * baseplate.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 * Lindstrøm</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef PIPINGMODULE_HPP
#define PIPINGMODULE_HPP

#include "Common.hpp"
#include "ModuleBase.hpp"

namespace s4 {
  namespace BasePlate {
        
    /**
     * @brief This module handles forwarding of messages to another context
     */
    class PipingModule : public ModuleBase {
      
    public:
      PipingModule(Context& context, peer_t id);
      virtual ~PipingModule();
      void fromBridge(const std::vector<int8_t> data);
      virtual void toBridge(const std::vector<int8_t> data) = 0;

    protected:
      void start();

    private:
      /// @brief Opaque wrapper of the private parts.
      class Underpants;
      
      /// @brief Container of the private parts.
      Underpants *underpants;
    };
    
  }
}

#endif /* PIPINGMODULE_HPP */
