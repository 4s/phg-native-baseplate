Baseplate implementation for native modules
===========================================
This is the baseplate implementation for native modules that will
be executed on the "bare metal".

Documentation
-------------
This is an early prototype and not much documentation is available
at this point apart from inline comments in the source code.

Status
------
The native baseplate is fully functional, but not complete in the 
sense that sending and receiving messages require some work that
was intended to have been performed by the baseplate.

The following is also implemented:
- Master module
- Piping module
- Reflector Context
- Log module

Currently the Java log module is used instead of this C module.

To get a feel for how the baseplate works and should be used read the
tests in the test folder in the root of the project.

Building
--------
Our C based projects are build using conan (conan.io) and dependencies
are pulled from bintray using conan.

It is possible to build using standard conan commands.

Dependencies
------------
The baseplate require phg-messages to produce messages. zmq to route 
messages, and protobuf.

These dependencies are pulled automatically by conan.

Publishing
----------
We have created a script that can be called to build and publish to 
bintray as 4s:
```bash
./publish
```
The variables BT_PASSWORD and BT_USER needs to be set for the script to 
work.

Compiling requires conan, android-sdk and ndk, these tools has been installed
in a docker-hub image for convnience:
```
alexjesper/conan-android-ndk
```
The version of the package is defined in the conan file.

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.

License
-------
The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0