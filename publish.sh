#!/bin/bash
conan user -p "$BT_PASSWORD" -r 4s "$BT_USER" || exit 1
for buildtype in Release Debug
do
  echo
  echo "####### ${buildtype}:DefaultProfile"
  echo
  conan create . -s build_type=${buildtype}  || exit 1
  for abi in armeabi-v7a arm64-v8a x86 x86_64
  do
    echo
    echo "####### ${buildtype}:${abi}"
    echo
    conan create . --profile android_${abi}_clang --test-folder=None -e CONAN_RUN_TESTS=False -s os.api_level=26 -s build_type=${buildtype} || exit 1
  done
done
conan upload phg-native-baseplate -cr 4s --all
