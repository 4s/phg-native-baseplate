/*
 *   Copyright 2018 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Unit tests for 4s BasePlate system.
 *
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas Lindstrøm</a>,
 * The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "gtest/gtest.h"
#include "BasePlate.hpp"
#include "TestMessage.pb.h"

#include <thread>
#include <chrono>

using s4::messages::TestMessage;

TEST(Test_topic_format, Unicast) {
  using namespace s4::BasePlate;
  auto topic = Topic::unicast("A", "FUNCTION", "SIGNAL", "B", "C");
  ASSERT_TRUE(topic.asString() == "UA_FUNCTION_SIGNAL_B_C");

  topic = Topic::unicast("A", "FUNCTION", "SIGNAL", "B");
  string s = topic.asString();
  ASSERT_TRUE(s == "UA_FUNCTION_SIGNAL_B_");

  Topic parsed = Topic::fromString(s);
  ASSERT_TRUE(parsed.type == UNICAST);
  ASSERT_TRUE(parsed.destination == "A");
  ASSERT_TRUE(parsed.function == "FUNCTION");
  ASSERT_TRUE(parsed.signal == "SIGNAL");

  auto subTopic = Topic::subscribeUnicast("A");
  ASSERT_TRUE(subTopic.asString() == "UA_");
  ASSERT_TRUE(topic.asString().find(subTopic.asString(), 0) == 0);
}

TEST(Test_topic_format, Multicast) {
  using namespace s4::BasePlate;
  auto topic = Topic::multicast("FUNCTION", "SIGNAL", "B", "C");
  ASSERT_TRUE(topic.asString() == "M_FUNCTION_SIGNAL_B_C");
    
  topic = Topic::multicast("FUNCTION", "SIGNAL", "B");
  ASSERT_TRUE(topic.asString() == "M_FUNCTION_SIGNAL_B_");
    
  auto subTopic = Topic::subscribeMulticast("FUNCTION");
  ASSERT_TRUE(subTopic.asString() == "M_FUNCTION_");
  ASSERT_TRUE(topic.asString().find(subTopic.asString(), 0) == 0);
}
  
TEST(Test_topic_format, Broadcast) {
  using namespace s4::BasePlate;
  auto topic = Topic::broadcast("FUNCTION", "SIGNAL");
  ASSERT_TRUE(topic.asString() == "B_FUNCTION_SIGNAL_");
    
  auto subTopic = Topic::subscribeBroadcast();
  ASSERT_TRUE(subTopic.asString() == "B_");
  ASSERT_TRUE(topic.asString().find(subTopic.asString(), 0) == 0);
}

namespace s4 {
  namespace BasePlate {
    bool shutsDownByItself(ModuleBase* base) {
        int current_iteration = 1;
        int max_iterations = 10;
        int milliseconds_sleep = 30;

        do {
           std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds_sleep));
           if (current_iteration++ >= max_iterations) {
                return false;
            }
        }
        while(base->getApplicationState() != FINALIZING);

        return true;
    }
  }
}

/*
 * Test minimal baseplate setup.
 */
namespace s4 {
  namespace BasePlate {
    
    class Dummy : public ModuleBase {
    public:
      static std::atomic<bool> wasRunning;

      Dummy(Context& context) : ModuleBase(context, "Dummy") {
        wasRunning = false;
        addStateCallback(RUNNING,
                         [this]() -> void {
                           this->requestShutdown();
                           wasRunning = true;
                         });
        start();
      }

      void requestForceShutdown() {
        ModuleBase::requestShutdown(true);
      }
    };
    std::atomic<bool> Dummy::wasRunning(false);
  }
}

TEST(baseplate, Minimal_baseplate_setup) {
  using namespace s4::BasePlate;
  {
    Context context;
    Reflector reflector(&context);
    std::unique_ptr<ModuleBase> master(new MasterModule(context, 2));
    std::unique_ptr<Dummy> dummy(new Dummy(context));
    reflector.start();

    if(!shutsDownByItself(master.get())) {
      dummy->requestForceShutdown();
      FAIL() << "Baseplate not terminated within timeout";
    }
  }
  // This test starts a minimal baseplate with a dummy baseplate that once running, calls shut down
  // Here we ensure that the dummy module was running at some point
  ASSERT_TRUE(Dummy::wasRunning);
}

/*
 * Test send and receive protobuf messages
 */
namespace s4 {
  namespace BasePlate {
    
    class Receiver : public ModuleBase {
    public:
      static std::atomic<bool> hasReceivedAMessage;

      Receiver(Context &context) : ModuleBase(context, "Receiver") {
        hasReceivedAMessage = false;
        this->addFunction<TestMessage>("Function", [](MetaData metadata, TestMessage* input) -> void {
                                         ASSERT_TRUE(input->s() == "ABC");
                                         ASSERT_TRUE(input->n() == 37);
                                         ASSERT_TRUE(metadata.sender == "Sender");
                                         ASSERT_TRUE(metadata.signal == "MySignal");
                                         ASSERT_TRUE(metadata.messageType == UNICAST);
                                         // Ensures that the assertion has been executed
                                         hasReceivedAMessage = true;
                                       });
        
        start();
      }

      void requestForceShutdown() {
        ModuleBase::requestShutdown(true);
      }
     };
     std::atomic<bool> Receiver::hasReceivedAMessage(false);
    
    class Sender : public ModuleBase {
    public:
      Sender(Context &context) : ModuleBase(context, "Sender") {
        addStateCallback(RUNNING, [this]() -> void {
            TestMessage n;
            n.set_s("ABC");
            n.set_n(37);
            sendUnicast("Receiver", "Function", "MySignal", &n);
            requestShutdown();
          });
        start();
      }
    };
  }
}

TEST(baseplate, Simple_baseplate_setup) {
  using namespace s4::BasePlate;
  {
    Context context;
    Reflector reflector(&context);
    std::unique_ptr<ModuleBase> master(new MasterModule(context, 3));
    std::unique_ptr<ModuleBase> sender(new Sender(context));
    std::unique_ptr<Receiver> receiver(new Receiver(context));
    reflector.start();

    if(!shutsDownByItself(master.get())) {
      receiver->requestForceShutdown();
      FAIL() << "Baseplate not terminated within timeout";
    }
  }
  // At this point the baseplate has been shutdown (due to the unique_ptr)
  // We ensure that the message from "Sender" was received and asserted
  ASSERT_TRUE(Receiver::hasReceivedAMessage);
}

/*
 * Test shutdown cancellation.
 */
namespace s4 {
  namespace BasePlate {
    
    class Canceller : public ModuleBase {
    public:
      Canceller(Context& context, std::function<void()> callback) : ModuleBase(context, "Canceller") {
        addStateCallback(RUNNING, [this]() -> void {
            this->requestShutdown();
          });
        
        addStateCallback(SHUTDOWN_REQUEST, [callback]() -> void {
            callback();
          });
        
        setShutdownRequestHandler([this]() -> bool {
            return ++requests > 2; // Cancel the first two requests and accept the third.
          });
        
        start();
      }

      void requestForceShutdown() {
        ModuleBase::requestShutdown(true);
      }
      
    private:
      int requests = 0;
    };
  }
}

TEST(baseplate, Shutdown_cancellation) {
  using namespace s4::BasePlate;
  int calls = 0;
  {
    Context context;
    Reflector reflector(&context);
    
    std::unique_ptr<ModuleBase> master(new MasterModule(context, 2));
    std::unique_ptr<Canceller> canceller(new Canceller(context, [&calls]() -> void { calls++; }));
    
    reflector.start();

    if(!shutsDownByItself(master.get())) {
      canceller->requestForceShutdown();
      FAIL() << "Baseplate not terminated within timeout";
    }
  }
  ASSERT_TRUE(calls == 3);
}

namespace s4 {
  namespace BasePlate {
    
    class ForcedShutdown : public ModuleBase {
    public:
      ForcedShutdown(Context& context) : ModuleBase(context, "ForcedShutdown") {
        addStateCallback(RUNNING,
                         [this]() -> void {
                           this->requestShutdown(true);
                         });
        start();
      }
    };
    
    class FutileCanceller : public ModuleBase {
    public:
      static std::atomic<bool> hasHandlerBeenSet;
      static std::atomic<bool> hasShutdownBeenRequested;
      static std::atomic<bool> hasShutdownBeenFinalized;

      FutileCanceller(Context& context) : ModuleBase(context, "FutileCanceller") {
        hasHandlerBeenSet = false;
        hasShutdownBeenRequested = false;
        hasShutdownBeenFinalized = false;

        setShutdownRequestHandler([]() -> bool {
          hasHandlerBeenSet = true;
          return false;
        });
        
        addStateCallback(SHUTDOWN_REQUEST, []() -> void {
          hasShutdownBeenRequested = true;
        });

        addStateCallback(FINALIZING, []() -> void {
          hasShutdownBeenFinalized = true;
        });
        start();
      }

      void requestForceShutdown() {
        ModuleBase::requestShutdown(true);
      }
    };
    std::atomic<bool> FutileCanceller::hasHandlerBeenSet(false);
    std::atomic<bool> FutileCanceller::hasShutdownBeenRequested(false);
    std::atomic<bool> FutileCanceller::hasShutdownBeenFinalized(false);
  }
}

TEST(baseplate, Forces_shutdown) {
  using namespace s4::BasePlate;
  {
    Context context;
    Reflector reflector(&context);

    std::unique_ptr<ModuleBase> master(new MasterModule(context, 3));
    std::unique_ptr<ModuleBase> forced(new ForcedShutdown(context));
    std::unique_ptr<FutileCanceller> futile(new FutileCanceller(context));

    reflector.start();

    if(!shutsDownByItself(master.get())) {
      futile->requestForceShutdown();
      FAIL() << "Baseplate not terminated within timeout";
    }
  }
  ASSERT_FALSE(FutileCanceller::hasHandlerBeenSet);
  ASSERT_FALSE(FutileCanceller::hasShutdownBeenRequested);
  ASSERT_TRUE(FutileCanceller::hasShutdownBeenFinalized);
}

/*
 * Test application states.
 */
namespace s4 {
  namespace BasePlate {
    
    class StateTester : public ModuleBase {
    public:
      ApplicationState previousState;
      
      StateTester(Context& context) : ModuleBase(context, "Canceller") {
        
        addStateCallback(INITIALIZING, [this]() -> void {
            previousState =  INITIALIZING;
          });
        
        addStateCallback(READY, [this]() -> void {
            ASSERT_TRUE(previousState == INITIALIZING);
            previousState = READY;
          });
        
        addStateCallback(RUNNING, [this]() -> void {
            ASSERT_TRUE(previousState == READY);
            previousState = RUNNING;
            requestShutdown();
          });
        
        addStateCallback(SHUTDOWN_REQUEST, [this]() -> void {
            ASSERT_TRUE(previousState == RUNNING);
            previousState = SHUTDOWN_REQUEST;
          });
        
        addStateCallback(FINALIZING, [this]() -> void {
            ASSERT_TRUE(previousState == SHUTDOWN_REQUEST);
          });
        
        start();
      }

      void requestForceShutdown() {
        ModuleBase::requestShutdown(true);
      }
    };
  }
}

TEST(baseplate, Application_states) {
  using namespace s4::BasePlate;
  {
    Context context;
    Reflector reflector(&context);
    
    std::unique_ptr<ModuleBase> master(new MasterModule(context, 2));
    std::unique_ptr<StateTester> state_tester(new StateTester(context));
    
    reflector.start();

    if(!shutsDownByItself(master.get())) {
      state_tester->requestForceShutdown();
      FAIL() << "Baseplate not terminated within timeout";
    }
  }
  // At this point the baseplate has been shutdown and assertions
  // has been executed.
}

/*
 * Test error handling
 */
namespace s4 {
  namespace BasePlate {
    
    std::string msg("SOMETHING WENT WRONG");
    int code = 42;
    
    class FaultyReceiver : public ModuleBase {
    public:
      FaultyReceiver(Context &context) : ModuleBase(context, "Receiver") {
        
        addFunction("MyFunction", [](MetaData metadata, Callback, ErrorCallback error) -> void {
          error(code, msg);
        });
        
        start();
      }
    };
    
    class ErrorHandlingSender : public ModuleBase {
    public:
      static std::atomic<int> errorCode;
      static string errorMsg;
      static std::atomic<bool> succesCalled;

      ErrorHandlingSender(Context &context) : ModuleBase(context, "Sender") {
        errorCode = -1;
        errorMsg = "Hest";
        succesCalled = false;

        addStateCallback(RUNNING, [this]() -> void {
            sendUnicastWithCallback<TestMessage>("Receiver", "MyFunction", [](TestMessage* m) -> void {
                succesCalled = true;
              }, [this](int c, string m) -> void {
                errorMsg = m;
                errorCode = c;
                requestShutdown();
              });
          });
        start();
      }

      void requestForceShutdown() {
        ModuleBase::requestShutdown(true);
      }
    };

    std::atomic<int> ErrorHandlingSender::errorCode(-1);
    string ErrorHandlingSender::errorMsg = "Hest";
    std::atomic<bool> ErrorHandlingSender::succesCalled(false);
  }
}

TEST(baseplate, Error_handling) {
  using namespace s4::BasePlate;
  {
    Context context;
    Reflector reflector(&context);
    
    std::unique_ptr<ModuleBase> master(new MasterModule(context, 3));
    std::unique_ptr<ErrorHandlingSender> error_handling_sender(new ErrorHandlingSender(context));
    std::unique_ptr<ModuleBase> faultyReceiver(new FaultyReceiver(context));

    reflector.start();

    if(!shutsDownByItself(master.get())) {
      error_handling_sender->requestForceShutdown();
      FAIL() << "Baseplate not terminated within timeout";
    }
  }
  ASSERT_FALSE(ErrorHandlingSender::succesCalled);
  ASSERT_TRUE(ErrorHandlingSender::errorCode == 42);
  ASSERT_TRUE(ErrorHandlingSender::errorMsg.compare("SOMETHING WENT WRONG") == 0);
}

/*
 * Test callbacks
 */
namespace s4 {
  namespace BasePlate {
    
    class CallbackReceiver : public ModuleBase {
    public:
      CallbackReceiver(Context& context) : ModuleBase(context, "Receiver") {
        
        addFunction("MyFunction", [](MetaData metadata, Callback callback, ErrorCallback error) -> void {
          TestMessage m;
          m.set_n(42);
          callback(&m);
        });
        
        start();
      }

      void requestForceShutdown() {
        ModuleBase::requestShutdown(true);
      }
    };
    
    class CallbackSender : public ModuleBase {
    public:
      static std::atomic<int> callbackValue;

      CallbackSender(Context& context) : ModuleBase(context, "Sender") {
        callbackValue = -1;
        addStateCallback(RUNNING, [this]() -> void {
          sendUnicastWithCallback<TestMessage>("Receiver", "MyFunction", [this](TestMessage* m) -> void {
            callbackValue = m->n();
            requestShutdown();
          }, [](int code, string msg) -> void {
            // Should not happen
          });
        });
        start();
      }
    };
    std::atomic<int> CallbackSender::callbackValue(-1);
  }
}

TEST(baseplate, Callback) {
  using namespace s4::BasePlate;
  {
    Context context;
    Reflector reflector(&context);
    
    std::unique_ptr<ModuleBase> master(new MasterModule(context, 3));
    std::unique_ptr<ModuleBase> callback_sender(new CallbackSender(context));
    std::unique_ptr<CallbackReceiver> callback_receiver(new CallbackReceiver(context));

    reflector.start();

    if(!shutsDownByItself(master.get())) {
      callback_receiver->requestForceShutdown();
      FAIL() << "Baseplate not terminated within timeout";
    }
  }
  ASSERT_TRUE(CallbackSender::callbackValue == 42);
}

/*
 * Test repeated callbacks
 */
namespace s4 {
  namespace BasePlate {
    
    class RepeatedReceiver : public ModuleBase {
    public:
      RepeatedReceiver(Context& context) : ModuleBase(context, "Receiver") {
        
        addFunction("MyFunction", [](MetaData metadata,
                                     Callback callback,
                                     ErrorCallback error) -> void {
                      TestMessage m;
                      m.set_n(78);
                      callback.keepAlive()(&m);
          
                      std::this_thread::sleep_for(std::chrono::milliseconds(100));
                      m.set_n(621);
                      callback.keepAlive()(&m);
          
                      std::this_thread::sleep_for(std::chrono::milliseconds(100));
                      m.set_n(42);
                      callback(&m);
                    });
        start();
      }
    };
    
    class RepeatedSender : public ModuleBase {
    public:
      int index = 0;
      static int values[];

      RepeatedSender(Context& context) : ModuleBase(context, "Sender") {
        values[0] = 0;
        values[1] = 0;
        values[2] = 0;
        addStateCallback(RUNNING, [this]() -> void {
            sendUnicastWithCallback<TestMessage>("Receiver", "MyFunction", [this](TestMessage* m) -> void {
                values[index++] = m->n();
                if (index == 2) {
                  requestShutdown();
                }
              }, [](int code, string msg) -> void {
                // Should not happen
              });
          });
        start();
      }

      void requestForceShutdown() {
        ModuleBase::requestShutdown(true);
      }
    };
    int RepeatedSender::values[] = {0, 0, 0};
  }
}

TEST(baseplate, Test_repeated_callback) {
  using namespace s4::BasePlate;
  {
    Context context;
    Reflector reflector(&context);
    std::unique_ptr<ModuleBase> masterModule(new MasterModule(context, 3));
    std::unique_ptr<RepeatedSender> repeatedSender(new RepeatedSender(context));
    std::unique_ptr<ModuleBase> repeatedReceiver(new RepeatedReceiver(context));
    reflector.start();

    if (!shutsDownByItself(masterModule.get())) {
        repeatedSender->requestForceShutdown();
        FAIL() << "Baseplate not terminated within timeout";
    }
  }
  ASSERT_TRUE(RepeatedSender::values[0] == 78);
  ASSERT_TRUE(RepeatedSender::values[1] == 621);
  ASSERT_TRUE(RepeatedSender::values[2] == 42);
}