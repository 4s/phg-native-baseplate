#include <iostream>
#include <gtest/gtest.h>
#include "ModuleBase.hpp"

const char *helloWorld = "Hello world!";

TEST(Modulebase, HelloWorld) {
    EXPECT_STREQ(helloWorld, "Hello world!");
}
