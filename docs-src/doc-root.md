
Introduction     {#mainpage}
============

\if 0
******************************************************************************
***                                                                        ***
***     This file contains the first (and main) page of the 4SDC           ***
***     documentation, with the introduction and documentation overview.   ***
***                                                                        ***
***        AUTHOR:   Jacob Andersen (C) The Alexandra Institute 2015       ***
***        LICENSE:  Apache 2.0                                            ***
***                                                                        ***
******************************************************************************
\endif


The 4S Device Communication module collection (4SDC) is a cross
platform library for applications interacting with personal health
devices (like blood pressure monitors, oximeters, thermometers, weight
scales and so on).

4SDC comprises a cross-platform library written in C++ which is able
to communicate through a Platform Abstraction Layer (PAL) with
platform-specific modules.


\if full

This manual is aimed at contributors to the 4SDC module collection,
and contains the full and hairy implementation details. 

\else 

This manual is aimed at developers who are either going to use modules
from the 4SDC module collection or need a general technical overview
of the 4SDC architecture. The content of this manual will therefore
not cover implementation details.

\endif


\htmlonly
This manual is also available as a PDF file.
\endhtmlonly
\latexonly
This manual is also available as interactive web pages.
\endlatexonly
Please refer to [www.4s-online.dk/4SDC/Documentation]
(http://www.4s-online.dk/4SDC/Documentation)
for the relevant documentation.


\if full

\note 
If you are not intending to modify any of the 4SDC modules, you are
encouraged instead to look at the *Interface Documentation*, which is
aimed at developers who are only going to use the modules but have no
need for the internal implementation details (as well as other people,
who just need the essential overview). The *Interface Documentation*
is also the recommended place to start for new contributors, who wish
to get an overview of the 4SDC module collection without too much
"noise".

\note
The latest version of the *Interface Documentation* can be found here:

\note
[www.4s-online.dk/4SDC/Documentation/latest/interface/]
(http://www.4s-online.dk/4SDC/Documentation/latest/interface/)

\else

\note
Developers already familiar with the architecture, who are planning to
contribute to the 4SDC modules, are encouraged to head over to the
*Full Documentation*, which contains all the hairy implementation
details.

\note
The latest version of the *Full Documentation* can be found here:

\note
[www.4s-online.dk/4SDC/Documentation/latest/full/]
(http://www.4s-online.dk/4SDC/Documentation/latest/full/)

\endif


Documentation Overview     {#sec_overview}
======================

\htmlonly
This manual is organised with a number of introductory pages followed
by detailed information about each of the modules, classes and
files. The pages can be navigated by using the tree-view on the left
of your screen (and of course by following the hyperlinks). If you
wish to print a hardcopy of the documentation instead, we suggest
printing from the PDF file instead, as it is optimised for printing.
\endhtmlonly
\latexonly
This manual is organised with a number of introductory chapters
followed by chapters with detailed information about each of the
modules, classes and files. The detailed information in the last
(huge) chapters may be easier to navigate on the interactive web pages
version of the manual.
\endlatexonly

If you are new to the world of 4SDC, we recommend that you read the
\ref page_start "Getting Started"
\htmlonly
page, followed by the pages
\endhtmlonly
\latexonly
chapter on page \pageref{page_start}, and continue through the
following chapters
\endlatexonly
about the 4SDC architecture.


Other Sources of Information     {#sec_external_docs}
============================

The documentation found
\htmlonly
on these pages
\endhtmlonly
\latexonly
in this document
\endlatexonly
is generated from the source code of a certain version of the 4SDC
library, and is therefore rather static in nature. However, there are
a few other sources of 4SDC information, you should be aware of -- all
of them are more dynamic, and of course you are invited to contribute
as well:
\addindex Wiki
<dl><dt>Wiki</dt>
<dd>The 4SDC Wiki page at
[4s-online.dk/wiki/doku.php?id=4sdc:]
(http://4s-online.dk/wiki/doku.php?id=4sdc:) is used first of
all as a general overview over the 4SDC module collection and secondly
as the tutorial on how to get the demo application up and running -- on
various platforms.</dd>
\addindex Issuetracker
\addindex Jira
<dt>Issuetracker</dt>
<dd>The issuetracker at [issuetracker4s.atlassian.net/projects/SDC/]
(https://issuetracker4s.atlassian.net/projects/SDC/) is used to report
bugs and feature requests, and to coordinate the work on the 4SDC
modules.</dd>
\addindex Forum
<dt>Forum</dt>
<dd>The forum at [4s-online.dk/forum/] (http://4s-online.dk/forum/) is
used for Q/A and discussions.</dd></dl>

\if 0
******************************************************************************
***                                                                        ***
***    Below the order of the remaining chapters of the documentation      ***
***    is defined                                                          ***
***                                                                        ***
******************************************************************************
\endif


\page      page_start          Getting Started
The Baseplate allows the different parts of an app (modules) to communicate asynchonously while ensuring that the implementation of each module can be done very simple. A module is implemented as number of functions that other modules can call. 

Create a module
===============

A module is a subclass of s4::BasePlate::ModuleBase and functions to be exposed to other modules is added using s4::BasePlate::ModuleBase::addFunction. Below is a simple module implementation:

\code{cpp}
class MyModule : public s4::BasePlate::ModuleBase {
public:
	MyModule(s4::BasePlate::Context* context) : ModuleBase(context, "MyModule") {

  		addFunction("MyFunction", [](s4::BasePlate::MetaData metadata) -> void {
    		// Do something
        });
        
        start(); // Start this module
    }
};
\endcode

The module is started by calling s4::BasePlate::ModuleBase::start() which can be done at the end of the constructor or after the module is constructed.

Starting the baseplate
======================
The baseplate handles startup and shutdown of modules and their communication. The baseplate is represented by an instance of s4::BasePlate::BasePlate which has to be created and then started using s4::BasePlate::BasePlate::run. The following code will create two modules of type `MyModule1` and `MyModule2`, both which needs to be subclasses of s4::BasePlate::ModuleBase and run the baseplate and the two modules until they are all shutdown.

\code{cpp}
BasePlate baseplate();
baseplate.run({new MyModule1(baseplate.context), new MyModule2(baseplate.context)}).
\endcode

The above code will also create a default logger which logs to `stderr`. See also #logging.

Calling other modules
=====================

Another module can call a on another module by either executing s4::BasePlate::ModuleBase::sendUnicast`("MyModule", "MyFunction")` which will call `MyFunction` on the module `MyModule` or s4::BasePlate::ModuleBase::sendMulticast`("MyFunction")` which will call <i>all</i> functions with the name `MyFunction` on any module connected to the baseplate.

Note that a module may add functions after s4::BasePlate::ModuleBase::start has been called, but these functions can only be called using unicasts and <i>not</i> multicasts.

Payload
-------

Function calls may contain a payload represented by an instance of a <a href="https://developers.google.com/protocol-buffers/">Google Protocol Buffers</a> message. The module exposing the function that expects a payload must know the type of the message. Say we have a class called `MyMessage` which is a subclass of google::protobuf::Message. We can now add a function expecting a payload of this type

\code{cpp}
addFunction("MyFunction", [](s4::BasePlate::MetaData metadata, MyMessage payload) -> void {
	// Do something with the payload
});
\endcode

The payload now has to be provided in the call by adding the payload as parameter to s4::BasePlate::ModuleBase::sendUnicast or s4::BasePlate::ModuleBase::sendMulticast, eg. \code sendUnicast("MyModule", "MyFunction", payload) \endcode

Callback
--------
If a response is expected from a function call, the calling module can specify a callback method. This is done by using the s4::BasePlate::ModuleBase::sendUnicastWithCallback or s4::BasePlate::ModuleBase::sendMulticastWithCallback methods. Say that we use the same call as above, but  `MyFunction` should return a messge of type `MyCallbackMessage`. Then we add `MyFunction` as

\code{cpp}
addFunction("MyFunction", [](s4::BasePlate::MetaData metadata, 
							 MyMessage payload,
							 s4::BasePlate::Callback callback,
							 s4::BasePlate::ErrorCallback error) -> void {
	// Do something and create callbackMessage
	callback(callbackMessage);
});
\endcode

Each module owns a single thread and all calls to its functions are done on this thread.

The calling module should now use s4::BasePlate::ModuleBase::sendUnicastWithCallback and add callback and error callback to the call:

\code{cpp}
sendUnicastWithCallback<MyCallbackMessage>("Receiver", "MyFunction", 
	[](MyMessageCallback* m) -> void {
		// We got a response
	}, [](int code, string msg) -> void {
		// Something went wrong
	});
\endcode

If somethings goes wrong, `MyFunction` can call the error callback, `error` with an error code and an error message, and the error callback will be called instead of the callback.

\code{cpp}
addFunction("MyFunction", [](s4::BasePlate::MetaData metadata, 
							 MyMessage payload,
							 s4::BasePlate::Callback callback,
							 s4::BasePlate::ErrorCallback error) -> void {
	// Something went wrong - notify the caller
	error(code, message);
});
\endcode

By default, callback functions can only be called once, but if it has to be used multiple times, the responding module should call s4::BasePlate::Callback::keepAlive on the callback:
\code
callback.keepAlive()(callbackMessage);
\endcode
Before the callback is called the last time, s4::BasePlate::Callback::last should be called. This will cause the callback to be removed after the next call. The same applies for the error callback.

State changes
=============
A module can be in one of a number of <i>states</i>:
<table>
<tr><th>State<th>Description
<tr><td>`INITIALIZING`<td>The module is starting up and may not be connected to the baseplate.
<tr><td>`READY`<td>The module is connected to the baseplate and is ready to run. A module may receive messages in this state, but it is not safe to send messages to other modules.
<tr><td>`RUNNING`<td>The module is running and may call other modules.
<tr><td>`SHUTDOWN_REQUEST`<td>A module has requested the baseplate to be shut down. No more communication is allowed.
<tr><td>`FINALIZING`<td>The baseplate is shutting down and the module should do so too.
</table>
A module may only call other modules when in `RUNNING`. A module can add listeneres to state changes by calling `addStateCallback` with a state to listen for and a callback method:

\code{cpp}
addStateCallback(RUNNING, []() -> void {
	// Do something when module is running
});
\endcode

Shutdown
========
A module can request the baseplate to shut down by calling the `requestShutdown` method with a boolean parameter indicating whether the shutdown should be forced. This changes the state to `SHUTDOWN_REQUEST`.

If the shutdown is <i>not</i> forced, modules may cancel the shutdown via its shutdown request handler. The handler is defined using the `setShutdownRequestHandler` method which takes one parameter: a callback function `std::function<bool(void)>`. This callback will be called when some module request a shutdown. If the callback returns `true` the module accepts the shutdown request and `false` if not:

\code{cpp}
setShutdownRequestHandler([]() -> bool {
	if (/* I'm ready to shut down */) {
		return true;
	} else {
		return false;
	}
});
\endcode

If a module does not accept the request, all other modules are notified and state is changed back to `RUNNING`.

Logging {#logging}
=======
Modules can log using the macros `dbg`, `info`, `warn`, `err` and `ftl`.

By default all log messages are printet to stderr by a module created by the baseplate, but modules can add a function with the name `Log` to receive log messages encoded using the protobuf message s4::BasePlate::LogMessage. 


\page      page_arch           Architecture

A basic application based on the baseplate consists of a number <i>modules</i> and a <i>reflector</i>. The functionality of the app is implemented in the modules and the reflector allows modules to communicate with each other. Each module and the reflector owns their own threads and they communicate using <a href="http://zeromq.org/">ZeroMQ</a> which make sure that modules receive exactly the messages that are relevant for them.

The modules are subclasses of s4::BasePlate::ModuleBase and they each expose a number of functions for other modules to call. Each module subscribes to a socket on the reflector and publishes to another socket. The reflector simply forwards all messages based on the subscriptions (see the ZeroMQ docs for details). Each module subscribes to all messages sent directly to it as unicasts and also all multicasts to all the functions it added <i>before</i> s4::BasePlate::ModuleBase::start was called.

In order so orchestrate startup and shutdown, a special module, the <i>Master Module</i> is run as part of the BasePlate (see s4::BasePlate::MasterModule). The Master Module exposes two functions, the <i>StartupMaster</i> and the <i>ShutdownMaster</i>.

Startup
-------
The Master Module needs to know the number of modules the BasePlate expects, and once started, startup master function listens for PING messages from all modules. A PING message is answered with a PONG message. When a module receives a PONG it, changes state to `READY` and sends a READY message to the Master Module. When all modules has sent a READY message to the Master Module, the Master Module broadcasts a START message. When a module receives the START message it changes state to `RUNNING` and the application is now running.

Shutdown
--------
Any module can request a shutdown by sending a `SHUTDOWN_REQUEST`message to the Master Module. All modules are notified of this request, change their state to `SHUTDOWN_REQUEST` and must reply either `READY` or `CANCEL`. If all modules reply with `READY` to the Master Module, a `FINALIZE` message is broadcasted to all modules who change their state to `FINALIZING`, and shut down their respective threads. If a module replies with `CANCEL` to the Master Module, the `CANCEL`-message is brodcasted to all modules and they change their state back to `RUNNING` resume running.

Piping
======
The baseplate may have multiple reflector, each with its own set of modules connected to it. However, only one is an actual reflector, and the others act as <i>bridges</i>. Bridges forward all messages (1-4) from its modules (without publishing them to its modules) to the reflector through a <i>Piping Module</i> connected to the reflector. The Piping Module subscibes to anything that the reflector publishes, and forwards (5-7) everything to the Bridge who now publishes to its modules (8).

Note that the communication between the Piping Module and the Bridge Module (dashed lines below) is specific to the use case, and needs to be implemented as a subclass of s4::BasePlate::PipingModule. 

\image html Piping.svg

Specifically, we use piping to use modules implemented in Java, with a Bridge also implemented in Java. Here, the communication between the Bridge Module and the Piping Module is done using JNI.

Topics/ØMQ
======
We use the PUB/SUB pattern in ZeroMQ (ØMQ), where modules subscribe to a number of prefixes. We define three types of topics for a message sent over the baseplate:

<table>
<tr><th>Type<th>Pattern<th>Description
<tr><td>Unicast<td>`U[RECEIVER]_[FUNCTION]_[SIGNAL]_[SENDER]_(CALLBACK_)?`<td>A message sent to a specific module (`RECEIVER`).
<tr><td>Multicast<td>`M_[FUNCTION]_[SIGNAL]_[SENDER]_(CALLBACK_)?`<td>A message sent to all functions with a given name.
<tr><td>Broadcast<td>`B_[FUNCTION]_[SIGNAL]_`<td>A message sent to all modules.
</table>

<table>
<tr><th>Parameter<th>Description
<tr><td>`RECEIVER`<td>The module to receive the unicast message.
<tr><td>`FUNCTION`<td>The function that to be called.
<tr><td>`SIGNAL`<td>A parameter that the caller can define and the called function can access through the MetaData parameter.
<tr><td>`SENDER`<td>The module sending this message.
<tr><td>`CALLBACK`<td>An optional callback which is the name of the function the calling module expects a response on.
</table>

\if full
\page      page_conformance    Conformance Information
\endif
\page      page_license        License
\page      page_contributors   Contributors


\defgroup  group_presentation  Presentation Layer
\defgroup  group_session       Session Layer
\defgroup  group_pal           Platform Abstraction Layer
\defgroup  group_system        System Layer
